/*
 * NAME: LCDDriver.h
 * AUTHOR: Adam Unger
 * DESCRIPTION: this file defines the public functions related to the LCD driver
 *  and related low level functionality.
 */

/*
 * Header guard...
 */
#ifndef LCD_DRIVER_
#define	LCD_DRIVER_

#include "RiffRaff.h" // Color, HIGH/LOW, DELAY_625NS

// The screen that we are controlling is 240x320 pixels and it will be used
// exclusively in portrait mode so these are flipped from data sheet
#define WIDTH_PIXELS 320
#define HEIGHT_PIXELS 240

/*
 * The "power" pin for the LED backlight. It could be PWM'd for fade in/out
 * effects.
 */
#define BACKLIGHT PORTCbits.RC2

/*
 * Set the LCD up for sane default values. This can be called at any time.
 * INPUT: none
 * OUTPUT: none
 */
void initLCD(void);

/*
 * Put the display into a low-power mode. The data sheet recommends at least 5ms
 * for voltages to stabilize before next command is sent and at least 120ms
 * before sleep out command is sent. To prevent Tek from finding another "bug"
 * we'll delay 120ms.
 * INPUT: none
 * OUTPUT: none
 */
void sleepLCD();

/*
 * Bring the display out of low-power mode. The data sheet recommends at least
 * 5ms for voltages to stabilize before next command is sent and at least 120ms
 * before sleep in command is sent. To prevent Tek from finding another "bug"
 * we'll delay 120ms.
 * INPUT: none
 * OUTPUT: none
 */
void sleepOutLCD();

/*
 * This will print the input color to every pixel contained within the
 * contiguous block as delimited by the input x and y coordinates. This assumes
 * the input x and y's are sorted least -> greatest.
 * INPUT: Color c - the color to make the line
 *        short x - left side of block to be filled
 *        short x1 - right side of block to be filled
 *        short y - top of block to be filled
 *        short y1 - bottom of block to be filled
 * OUTPUT: none
 */
inline void floodLCD(Color c, short x, short x1, short y, short y1);

///*
// * Read a pixel at the input x and y coordinates. The RGB values will be placed
// * into the input color.
// * INPUT: char x - column of pixel to read
// *        char y - row of pixel to read
// *        Color *c - pointer to a Color struct for the results
// */
//void readPixelLCD(char x, char y, Color *c);

#endif	/* LCD_DRIVER_ */

