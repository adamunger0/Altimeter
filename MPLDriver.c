/*
 * NAME: MPLDriver.c
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file defines all of the functionality for interacting with
 * the MPL3115A2.
 */

#include <xc.h>
#include "I2C.h"
#include "RiffRaff.h"
#include "stdbool.h"
#include "MPLDriver.h"

#define MPL3115A2_ADDRESS 0xC0 // I2C addr, already left shifted by 1

#define MPL3115A2_STATUS 0x0 // flags for data ready
// registers which contain temp, pres, and alt
#define MPL3115A2_P_MSB 0x1
#define MPL3115A2_P_CSB 0x2
#define MPL3115A2_P_LSB 0x3
#define MPL3115A2_T_MSB 0x4
#define MPL3115A2_T_LSB 0x5
#define MPL3115A2_SYSMOD_REG_MPL 0x11
#define MPL3115A2_PT_DATA_CFG 0x13 // event flag generator control
// register for equivalent sea level pressure at measurement location
#define MPL3115A2_BAR_IN_MSB 0x14
#define MPL3115A2_BAR_IN_LSB 0x15
#define MPL3115A2_CTRL_1 0x26 // general config
#define MPL3115A2_OFF_T 0x2C // temperature offset
#define MPL3115A2_OFF_H 0x2D // altitude offset
#define MPL3115A2_OFF_P 0x2B // pressure offset

// masks for setting/clearing various configuration bits
#define OST_MASK 0x2
#define PDR_MASK 0x4

/*
 * Constants to be used for unit calculations.
 */
#define METRIC_ALT_CONSTANT 65536.0
#define IMPERIAL_ALT_CONSTANT 19975.372160788
#define METRIC_TEMP_CONSTANT 256.0
#define IMPERIAL_TEMP_CONSTANT 142.222222222


/*
 * Reads a single byte from the MPL. Kicks of with a start command.
 * INPUT: char reg - register to read
 * OUTPUT: char - byte from input register
 */
static char readByteMPL(char reg) {
    i2cStart(MPL3115A2_I2C_MODULE);
    i2cWrite(MPL3115A2_I2C_MODULE, MPL3115A2_ADDRESS | I2C_WRITE_CMD);
    i2cWrite(MPL3115A2_I2C_MODULE, reg);
    i2cRepStart(MPL3115A2_I2C_MODULE);
    i2cWrite(MPL3115A2_I2C_MODULE, MPL3115A2_ADDRESS | I2C_READ_CMD);
    char data = i2cRead(MPL3115A2_I2C_MODULE);
    i2cMasterAck(MPL3115A2_I2C_MODULE, I2C_NOACK);
    return data;
}

/*
 * Reads a single byte from the MPL. Kicks of with a repeated start command.
 * INPUT: none
 * OUTPUT: char - byte from input register
 */
static char readByteMPLRepStart() {
    i2cRepStart(MPL3115A2_I2C_MODULE);
    i2cWrite(MPL3115A2_I2C_MODULE, MPL3115A2_ADDRESS | I2C_READ_CMD);
    char data = i2cRead(MPL3115A2_I2C_MODULE);
    i2cMasterAck(MPL3115A2_I2C_MODULE, I2C_NOACK);
    return data;
}

/*
 * Send a byte to a register on the MPL.
 * INPUT: char reg - register to write too
 *        char data - byte to place in the register
 * OUTPUT: none
 */
static void sendByteMPL(char reg, char data) {
    // I2C start condition
    i2cStart(MPL3115A2_I2C_MODULE);
    // Write I2C OP Code
    i2cWrite(MPL3115A2_I2C_MODULE, MPL3115A2_ADDRESS | I2C_WRITE_CMD);
    // Write to which register
    i2cWrite(MPL3115A2_I2C_MODULE, reg);
    // Data for register
    i2cWrite(MPL3115A2_I2C_MODULE, data);
    i2cStop(MPL3115A2_I2C_MODULE);
}

/*
 * Toggle one shot mode. This will tell the MPL to take a reading immediately.
 * Call this to tell the MPL to acquire fresh data. Once this returns, the
 * caller needs to query the status register to determine when the sample is
 * ready.
 * INPUT: none
 * OUTPUT: none
 */
static void toggleOST() {
    // get the control reg byte so we can update its OST bit
    char settings = readByteMPL(MPL3115A2_CTRL_1);
    // set OST bit high
    BITMASK_SET(settings, OST_MASK);
    // write the updated value to the mpl
    sendByteMPL(MPL3115A2_CTRL_1, settings);

    // now set the bit low
    BITMASK_CLEAR(settings, OST_MASK);
    // write the updated value to the mpl
    sendByteMPL(MPL3115A2_CTRL_1, settings);
}

/*
 * Place the MPL into standby mode.
 * INPUT: none
 * OUTPUT: none
 */
void offMPL() {
    sendByteMPL(MPL3115A2_SYSMOD_REG_MPL, 0);
}

/*
 * Set the MPL to a default state. This includes polling mode, active mode, OSR
 * of 128, and in altimeter mode.
 * INPUT: none
 * OUTPUT: none
 */
void initMPL() {
    sendByteMPL(MPL3115A2_SYSMOD_REG_MPL, MPL3115A2_P_MSB);
    // Configure the MPL3115A2
    //  bit0: SBYB - 1 - place into active mode
    //  bit1: OST - 0 - no one shot toggle
    //  bit2: RST - 0 - no reset
    //  bit3-5: OSR - 111 - set at 128
    //  bit6: RAW - 0 - no raw data
    //  bit7: ALT - 1 - initialize in altimeter mode
    // 10111001
    sendByteMPL(MPL3115A2_CTRL_1, 0b10111001);
    // Enable event flags for data ready events
    //  bit0: TDEFE - 1 - raise new temp data flag when ready
    //  bit1: PDEFE - 1 - raise new press/alt data flag when ready
    //  bit2: DREM - 0 - event flags raised whenever system acquires a new set
    //                   of data
    // xxxxx011
    sendByteMPL(MPL3115A2_PT_DATA_CFG, 0b00000011);
}

/*
 * Block until the status register indicates that a fresh sample is ready. This
 * should only be called after a OST has been initiated, otherwise it may end in
 * and infinite loop.
 * INPUT: none
 * OUTPUT: none
 */
static void waitStatusReady() {
    char stat = 0;
    do {
        stat = readByteMPL(MPL3115A2_STATUS);
        i2cStop(MPL3115A2_I2C_MODULE);
    } while (!BITMASK_CHECK(stat, PDR_MASK));
}

/*
 * Read the altitude and temperature data from the MPL. The data is written to
 * the input MPL_DATA struct. The data is converted to the input units.
 * INPUT: MPL_DATA *data - pointer to struct to hold the results of the read
 *        char units - METRIC or IMPERIAL
 * OUTPUT: none
 */
void readMPL(MPL_DATA *data, char units) {
    toggleOST();

    waitStatusReady();

    // now read the altitude data - this also clears the PDR flag - algorithm
    // from pg 21 of data sheet
    long msba = ((long) readByteMPL(MPL3115A2_P_MSB)) << 24;
    msba |= ((long) readByteMPLRepStart()) << 16;
    msba |= ((long) readByteMPLRepStart()) << 8;

    // now read the temperature data - algorithm from pg 21 of data sheet
    short msbt = ((short) readByteMPLRepStart()) << 8;
    msbt |= (short) readByteMPLRepStart();

    // end this I2C transaction
    i2cStop(MPL3115A2_I2C_MODULE);

    // convert the newly acquired data to appropriate units
    if (METRIC_UNITS == units) {
        data->altitude = (float) msba / METRIC_ALT_CONSTANT;
        data->temp = (float) msbt / METRIC_TEMP_CONSTANT;
    }
    // 1m = 3.28084ft, Tf = 9Tc/5 + 32
    else if (IMPERIAL_UNITS == units) {
        data->altitude = (float) msba / IMPERIAL_ALT_CONSTANT;
        data->temp = ((float) msbt / IMPERIAL_TEMP_CONSTANT) + 32;
    }
}

/*
 * Reads only the altitude from the MPL. The returned value has been converted
 * to the input units.
 * INPUT: char units - METRIC or IMPERIAL
 * OUTPUT: float - the altitude in meters or feet
 */
float readAltitudeMPL(char units) {
    toggleOST();

    waitStatusReady();

    // now read the altitude data - this also clears the PDR flag - algorithm
    // from pg 21 of data sheet
    long msb = ((long) readByteMPL(MPL3115A2_P_MSB)) << 24;
    msb |= ((long) readByteMPLRepStart()) << 16;
    msb |= ((long) readByteMPLRepStart()) << 8;

    // end this I2C transaction
    i2cStop(MPL3115A2_I2C_MODULE);

    float result;

    // convert the newly acquired data to appropriate units
    if (METRIC_UNITS == units) result = (float) msb / METRIC_ALT_CONSTANT;
    // 1m = 3.28084ft, Tf = 9Tc/5 + 32
    else if (IMPERIAL_UNITS == units)
        result = (float) msb / IMPERIAL_ALT_CONSTANT;

    return result;
}

/*
 * Reads the altitude but doesn't convert to any units. The altitude is stored
 * on the MPL in 3 registers and this will simply update the input MPL_ALT_RAW
 * struct to reflect those values.
 * INPUT: MPL_ALT_RAW *alt - pointer to struct to contain raw register values
 * OUTPUT: none
 */
void readAltitudeRawMPL(MPL_ALT_RAW *alt) {
    toggleOST();

    waitStatusReady();

    // now read the altitude data - this also clears the PDR flag - algorithm
    // from pg 21 of data sheet
    alt->msb = readByteMPL(MPL3115A2_P_MSB);
    alt->csb = readByteMPLRepStart();
    alt->lsb = readByteMPLRepStart();

    // end this I2C transaction
    i2cStop(MPL3115A2_I2C_MODULE);
}

/*
 * Convert the raw altitude values to an altitude in the given units.
 * INPUT: MPL_ALT_RAW *alt - pointer to struct that contains raw altitude data
 *        char units - METRIC or IMPERIAL
 * OUTPUT: float - the altitude in m or ft
 */
float rawAltToFloatMPL(MPL_ALT_RAW *alt, char units) {
    long msb = ((long) alt->msb) << 24;
    msb |= ((long) alt->csb) << 16;
    msb |= ((long) alt->lsb) << 8;

    // convert the newly acquired data to appropriate units
    if (METRIC_UNITS == units) return (float) msb / METRIC_ALT_CONSTANT;
    // 1m = 3.28084ft, Tf = 9Tc/5 + 32
    else if (IMPERIAL_UNITS == units) return (float) msb / IMPERIAL_ALT_CONSTANT;

    return 0.0;
}

/*
 * Reads the temperature values in the MPL. These values are not converted to
 * anything.
 * INPUT: MPL_TEMP_RAW *temp - pointer to struct to hold the temp data
 * OUTPUT: none
 */
void readTemperatureRawMPL(MPL_TEMP_RAW *temp) {
    toggleOST();

    waitStatusReady();

    // now read the temperature data - algorithm from pg 21 of data sheet
    temp->msb = readByteMPL(0x4);
    temp->lsb = readByteMPLRepStart();

    // end this I2C transaction
    i2cStop(MPL3115A2_I2C_MODULE);
}

/*
 * Convert the input temperature data into engineering units.
 * INPUT: MPL_TEMP_RAW *temp - 2 bytes of raw data
 *        char units - METRIC or IMPERIAL
 * OUTPUT: float - the temperature in C or F
 */
float rawTempToFloatMPL(MPL_TEMP_RAW *temp, char units) {
    short msb = ((short) temp->msb) << 8;
    msb |= ((short) temp->lsb);
    // convert the newly acquired data to appropriate units
    if (METRIC_UNITS == units) return (float) msb / METRIC_TEMP_CONSTANT;
    // 1m = 3.28084ft, Tf = 9Tc/5 + 32
    else if (IMPERIAL_UNITS == units)
        return ((float) msb / IMPERIAL_TEMP_CONSTANT) + 32;

    return 0.0;
}