/*
 * NAME: LCDDriver.c
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file defines the drivers for the ILI9341 LCD controller.
 * These are the low level functions that will be used to draw graphics
 * primitives. Many of these have been defined as macros as an effort to keep
 * the speed to a maximum. The additional code size is irrelevant - there is
 * space to spare.
 */

#include "LCDDriver.h"
#include <xc.h>

// for the delay_ms calls - the micro is actually running at 64MHz, but the
// delay macro can't work with 64000000 so set 32000000 and double the calls
#define _XTAL_FREQ 32000000

// Logical representations of the pins that are related to the LCD screen
#define ILI9341_DATA_PINS PORTA
#define ILI9341_RD PORTCbits.RC0
#define ILI9341_LITE PORTCbits.RC2
#define ILI9341_WR PORTCbits.RC5
#define ILI9341_DC PORTCbits.RC6

// Constant for 18 bits per pixel in MCU mode
#define ILI9341_18_BIT_RGB 0x6

// registers
#define ILI9341_SOFTWARE_RESET 0x1
#define ILI9341_READ_POWER_MODE 0xA
#define ILI9341_ENTER_SLEEP 0x10
#define ILI9341_SLEEP_OUT 0x11
#define ILI9341_DISPLAY_OFF 0x28
#define ILI9341_DISPLAY_ON 0x29
#define ILI9341_COLUMN_ADDRESS_SET 0x2A
#define ILI9341_PAGE_ADDRESS_SET 0x2B
#define ILI9341_WRITE_MEMORY 0x2C
#define ILI9341_COLOR_SET 0x2D
#define ILI9341_READ_MEMORY 0x2E
#define ILI9341_MEMORY_ACCESS_CONTROL 0x36
#define ILI9341_PIXEL_FORMAT_SET 0x3A
#define ILI9341_WRITE_MEMORY_CONTINUE 0x3C
#define ILI9341_READ_MEMORY_CONTINUE 0x3E
#define ILI9341_WRITE_CABC_MIN_BRIGHTNESS 0x5E
#define ILI9341_READ_CABC_MIN_BRIGHTNESS 0x5F
#define ILI9341_READ_ID_1 0xDA
#define ILI9341_READ_ID_2 0xDB
#define ILI9341_MADCTL_MY 0x80
#define ILI9341_MADCTL_MX 0x40
#define ILI9341_MADCTL_MV 0x20
#define ILI9341_MADCTL_BGR 0x08

// various logical pin state definitions
#define ILI9341_RD_ACTIVE ILI9341_RD = LOW
#define ILI9341_RD_INACTIVE ILI9341_RD = HIGH
#define ILI9341_WR_ACTIVE ILI9341_WR = LOW
#define ILI9341_WR_INACTIVE ILI9341_WR = HIGH
#define ILI9341_DC_COMMAND ILI9341_DC = LOW
#define ILI9341_DC_DATA ILI9341_DC = HIGH

/*
 * Quick way to strobe the write pin.
 */
#define ILI9341_WR_STROBE { ILI9341_WR_ACTIVE;\
                            DELAY_625NS;\
                            ILI9341_WR_INACTIVE;\
                            DELAY_625NS;\
                          }

// quick and easy read a byte from the LCD
#define ILI9341_READ_BYTE(x) {\
    ILI9341_RD_ACTIVE;\
    DELAY_625NS;\
    x = ILI9341_DATA_PINS >> 2;\
    ILI9341_RD_INACTIVE;\
}

/*
 * These macros will inline writing to the ILI9341 registers for speed
 * improvements. This will increase the program size but this is a necessary
 * trade-off since running the ILI9341 in 8-bit parallel mode at 64MHz isn't
 * very fast. These macros also assume that the CS pin is already active and
 * that the RD pin is inactive.
 */
#define ILI9341_SEND_BYTE(d) { ILI9341_DATA_PINS = d;\
                               DELAY_625NS;\
                               ILI9341_WR_STROBE;\
                             }

#define ILI9341_SEND_0(c) { ILI9341_DC_COMMAND;\
                            DELAY_625NS;\
                            ILI9341_SEND_BYTE(c);\
                          }

#define ILI9341_SEND_1(c, d) {\
    ILI9341_DC_COMMAND;\
    DELAY_625NS;\
    ILI9341_SEND_BYTE(c);\
    ILI9341_DC_DATA;\
    DELAY_625NS;\
    ILI9341_SEND_BYTE(d);\
}

#define ILI9341_SEND_4(c, h, m, n, l) {\
    ILI9341_DC_COMMAND;\
    DELAY_625NS;\
    ILI9341_SEND_BYTE(c);\
    ILI9341_DC_DATA;\
    DELAY_625NS;\
    ILI9341_SEND_BYTE(h);\
    ILI9341_SEND_BYTE(m);\
    ILI9341_SEND_BYTE(n);\
    ILI9341_SEND_BYTE(l);\
}

// at 64MHz, 1 Fins = 62.5ns - define this for convenience
#define DELAY_625NS asm("NOP")

/*
 * Set the LCD up for sane default values. This can be called at anytime.
 * INPUT: none
 * OUTPUT: none
 */
void initLCD() {
    // reset so that the screen gets a fresh start
    ILI9341_SEND_0(ILI9341_SOFTWARE_RESET);

    // set the read and write pins to their default states
    ILI9341_WR_INACTIVE;
    DELAY_625NS;
    ILI9341_RD_INACTIVE;
    DELAY_625NS;

    // configure 18-bit rgb color and horizontal display orientation
    ILI9341_SEND_1(ILI9341_MEMORY_ACCESS_CONTROL, ILI9341_MADCTL_MX |
            ILI9341_MADCTL_MY | ILI9341_MADCTL_MV | ILI9341_MADCTL_BGR);
    ILI9341_SEND_1(ILI9341_PIXEL_FORMAT_SET, ILI9341_18_BIT_RGB);

    // ensure the display is on and not sleeping
    ILI9341_SEND_0(ILI9341_SLEEP_OUT);
    ILI9341_SEND_0(ILI9341_DISPLAY_ON);
}

/*
 * Delay the requisite amount of time for sleep in/out commands
 */
static void delay120ms() {
    __delay_ms(20);
    __delay_ms(20);
    __delay_ms(20);
    __delay_ms(20);
    __delay_ms(20);
    __delay_ms(20);
    __delay_ms(20);
    __delay_ms(20);
    __delay_ms(20);
    __delay_ms(20);
    __delay_ms(20);
    __delay_ms(20);
}

/*
 * Put the display into a low-power mode. The data sheet recommends at least 5ms
 * for voltages to stabilize before next command is sent and at least 120ms
 * before sleep out command is sent. To prevent Tek from finding another "bug"
 * we'll delay 120ms.
 * INPUT: none
 * OUTPUT: none
 */
void sleepLCD() {
    ILI9341_SEND_0(ILI9341_ENTER_SLEEP);
    delay120ms();
}

/*
 * Bring the display out of low-power mode. The data sheet recommends at least
 * 5ms for voltages to stabilize before next command is sent and at least 120ms
 * before sleep in command is sent. To prevent Tek from finding another "bug"
 * we'll delay 120ms.
 * INPUT: none
 * OUTPUT: none
 */
void sleepOutLCD() {
    ILI9341_SEND_0(ILI9341_SLEEP_OUT);
    delay120ms();
}

/*
 * Set the drawing window for any of the drawing functions contained in
 * LCDDriver.c. The ILI9341 will not print any pixels outside of this window.
 * It is assumed that the input coordinates are sorted, i.e. x <= x1, y <= y1
 * INPUT: short x - lh coordinate, column
 *        short x1 - rh coordinate, column
 *        short y - upper page coordinate
 *        short y1 - lower page coordinate
 * OUTPUT: none
 */
static void setDrawWindowLCD(short x, short x1, short y, short y1) {
    // adjust the end x and y since the ILI9341 will print inclusively and this
    // isn't the expected or intuitive behavior when setting window bounds
    x1 -= 1;
    y1 -= 1;
    // adjust the column
    ILI9341_SEND_4(ILI9341_COLUMN_ADDRESS_SET, x >> 8, x, x1 >> 8, x1);
    // adjust the row
    ILI9341_SEND_4(ILI9341_PAGE_ADDRESS_SET, y >> 8, y, y1 >> 8, y1);
}

/*
 * This will print the input color to every pixel contained within the
 * contiguous block as delimited by the input x and y coordinates. This assumes
 * the input x and y's are sorted least -> greatest.
 * INPUT: Color c - the color to make the line
 *        short x - left side of block to be filled
 *        short x1 - right side of block to be filled
 *        short y - top of block to be filled
 *        short y1 - bottom of block to be filled
 * OUTPUT: none
 */
inline void floodLCD(Color c, short x, short x1, short y, short y1) {
    setDrawWindowLCD(x, x1, y, y1);
    unsigned long num = (unsigned long) (x1 - x) * (unsigned long) (y1 - y);
    // if the colors are all the same, then we can save some instruction cycles
    // since the data pins only need to be set once
    if (c.r == c.g && c.g == c.b) {
        ILI9341_SEND_0(ILI9341_WRITE_MEMORY);
        // the 6bits of the color should be the MSB's of the data pins
        ILI9341_DATA_PINS = c.r << 2;
        ILI9341_DC_DATA;
        for (unsigned long i = 0; i < num; ++i) {
            ILI9341_WR_STROBE;
            ILI9341_WR_STROBE;
            ILI9341_WR_STROBE;
        }
    }
    // otherwise, if the r, g, and b components aren't the same, then we have
    // too add several more instruction cycles
    else {
        ILI9341_SEND_0(ILI9341_WRITE_MEMORY);
        ILI9341_DC_DATA;
        for (unsigned long i = 0; i < num; ++i) {
            // the 6bits of the color should be the MSB's of the data pins
            ILI9341_SEND_BYTE(c.r << 2);
            // the 6bits of the color should be the MSB's of the data pins
            ILI9341_SEND_BYTE(c.g << 2);
            // the 6bits of the color should be the MSB's of the data pins
            ILI9341_SEND_BYTE(c.b << 2);
        }
    }
}

///*
// * Read a pixel at the input x and y coordinates. The RGB values will be placed
// * into the input color.
// * INPUT: char x - column of pixel to read
// *        char y - row of pixel to read
// *        Color *c - pointer to a Color struct for the results
// */
//void readPixelLCD(char x, char y, Color *c) {
//    // set the draw window to the pixel that we are interested in
//    setDrawWindowLCD(x, x + 1, y, y + 1);
//
//    // send the command to read a pixels memory location
//    ILI9341_SEND_0(ILI9341_READ_MEMORY);
//
//    // ready for data now
//    ILI9341_DC_DATA;
//    DELAY_625NS;
//
//    // the first byte is a dummy read
//    ILI9341_RD_ACTIVE;
//    DELAY_625NS;
//    ILI9341_RD_INACTIVE;
//
//    // get the red component of this pixel
//    ILI9341_READ_BYTE(c->r);
//
//    // get the green component of this pixel
//    ILI9341_READ_BYTE(c->g);
//
//    // get the blue component of this pixel
//    ILI9341_READ_BYTE(c->b);
//}



