/*
 * NAME: PortInit.h
 * AUTHOR: Adam Unger
 * DESCRIPTION: The public interface for initializing the PIC18LF46K22 as per
 * the requirements of the altimeter project.
 */

/*
 * Header guard..
 */
#ifndef PORTINIT_H_
#define	PORTINIT_H_

#define REFRESH_CONSTANT_SLOW_L 0x24 // 1s
#define REFRESH_CONSTANT_SLOW_H 0xF4 // 1s
//#define REFRESH_CONSTANT_FAST_L 0x12 // 500ms
//#define REFRESH_CONSTANT_FAST_H 0x7A // 500ms
//#define REFRESH_CONSTANT_FAST_L 0x09 // 250ms
//#define REFRESH_CONSTANT_FAST_H 0x3D // 250ms
#define REFRESH_CONSTANT_FAST_L 0x84 // 125ms
#define REFRESH_CONSTANT_FAST_H 0x1E // 125ms

/*
 * This will setup the micro to function as expected for the altimeter project.
 * This includes a 64MHz clock speed, enter sleep mode on SLEEP(), init ports,
 * and init timers.
 */
void setupMicro(void);

#endif	/* PORTINIT_H_ */
