/*
 * NAME: PICSetup.h
 * AUTHOR: Adam Unger
 * DESCRIPTION: Setup the PIC18LF46K22 configuration bits as per the
 * requirements of this altimeter project.
 */

/*
 * Header guard...
 */
#ifndef PICSETUP_H_
#define	PICSETUP_H_

// PIC18LF46K22 Configuration Bit Settings

#include <xc.h> // __EEPROM_DATA
#include "RiffRaff.h" // METRIC_UNITS

// EEPROM initialization stuff - see pg 187 of xc8 data sheet - these macros
// write 8 bytes of data at a time - to write to the 16th and the 32nd eeprom
// memory locations we need the in between macro invocations
__EEPROM_DATA(METRIC_UNITS, 0, 0, 0, 0, 0, 0, 0);
//__EEPROM_DATA(0, 0, 0, 0, 0, 0, 0, 0);

// #pragma config statements should precede project file includes.

// CONFIG1H
// Oscillator Selection bits
#pragma config FOSC = INTIO67
// 4X PLL Enable (Oscillator used directly)
#pragma config PLLCFG = OFF
// Primary clock enable bit (Primary clock is always enabled)
#pragma config PRICLKEN = ON
// Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config FCMEN = OFF
// Internal/External Oscillator Switchover bit
// (Oscillator Switchover mode disabled)
#pragma config IESO = OFF

// CONFIG2L
// Power-up Timer Enable bit (Power up timer enabled)
#pragma config PWRTEN = ON
// Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only
// (SBOREN is disabled))
#pragma config BOREN = SBORDIS
// Brown Out Reset Voltage bits (VBOR set to 2.20 V nominal)
#pragma config BORV = 220

// CONFIG2H
// Watchdog Timer Enable bits (Watch dog timer is always disabled. SWDTEN has no
// effect.)
#pragma config WDTEN = OFF
// Watchdog Timer Postscale Select bits (1:32768)
#pragma config WDTPS = 32768

// CONFIG3H
// CCP2 MUX bit (CCP2 input/output is multiplexed with RB3)
#pragma config CCP2MX = PORTB3
// PORTB A/D Enable bit (PORTB<5:0> pins are configured as digital I/O on
// Reset)
#pragma config PBADEN = OFF
// P3A/CCP3 Mux bit (P3A/CCP3 input/output is mulitplexed with RE0)
#pragma config CCP3MX = PORTE0
// HFINTOSC Fast Start-up (HFINTOSC output and ready status are delayed by the
// oscillator stable status)
#pragma config HFOFST = OFF
// Timer3 Clock input mux bit (T3CKI is on RB5)
#pragma config T3CMX = PORTB5
// ECCP2 B output mux bit (P2B is on RC0)
#pragma config P2BMX = PORTC0
// MCLR Pin Enable bit (MCLR pin enabled, RE3 input pin disabled)
#pragma config MCLRE = EXTMCLR

// CONFIG4L
// Stack Full/Underflow Reset Enable bit (Stack full/underflow will not cause
// Reset)
#pragma config STVREN = OFF
// Single-Supply ICSP Enable bit (Single-Supply ICSP disabled)
#pragma config LVP = OFF
// Extended Instruction Set Enable bit (Instruction set extension and Indexed
// Addressing mode disabled (Legacy mode))
#pragma config XINST = OFF

// CONFIG5L
// Code Protection Block 0 (Block 0 (000800-003FFFh) not code-protected)
#pragma config CP0 = OFF
// Code Protection Block 1 (Block 1 (004000-007FFFh) not code-protected)
#pragma config CP1 = OFF
// Code Protection Block 2 (Block 2 (008000-00BFFFh) not code-protected)
#pragma config CP2 = OFF
// Code Protection Block 3 (Block 3 (00C000-00FFFFh) not code-protected)
#pragma config CP3 = OFF

// CONFIG5H
// Boot Block Code Protection bit (Boot block (000000-0007FFh) not code-
// protected)
#pragma config CPB = OFF
// Data EEPROM Code Protection bit (Data EEPROM not code-protected)
#pragma config CPD = OFF

// CONFIG6L
// Write Protection Block 0 (Block 0 (000800-003FFFh) not write-protected)
#pragma config WRT0 = OFF
// Write Protection Block 1 (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT1 = OFF
// Write Protection Block 2 (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT2 = OFF
// Write Protection Block 3 (Block 3 (00C000-00FFFFh) not write-protected)
#pragma config WRT3 = OFF

// CONFIG6H
// Configuration Register Write Protection bit (Configuration registers (300000-
// 3000FFh) not write-protected)
#pragma config WRTC = OFF
// Boot Block Write Protection bit (Boot Block (000000-0007FFh) not write-
// protected)
#pragma config WRTB = OFF
// Data EEPROM Write Protection bit (Data EEPROM not write-protected)
#pragma config WRTD = OFF

// CONFIG7L
// Table Read Protection Block 0 (Block 0 (000800-003FFFh) not protected from
// table reads executed in other blocks)
#pragma config EBTR0 = OFF
// Table Read Protection Block 1 (Block 1 (004000-007FFFh) not protected from
// table reads executed in other blocks)
#pragma config EBTR1 = OFF
// Table Read Protection Block 2 (Block 2 (008000-00BFFFh) not protected from
// table reads executed in other blocks)
#pragma config EBTR2 = OFF
// Table Read Protection Block 3 (Block 3 (00C000-00FFFFh) not protected from
// table reads executed in other blocks)
#pragma config EBTR3 = OFF

// CONFIG7H
// Boot Block Table Read Protection bit (Boot Block (000000-0007FFh) not
// protected from table reads executed in other blocks)
#pragma config EBTRB = OFF

#endif	/* PICSETUP_H_ */
