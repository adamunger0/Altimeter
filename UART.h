/*
 * NAME: UART.h
 * AUTHOR: Adam Unger
 * DESCRIPTION: This defines the interface for interactions with the UART
 * module.
 */

/*
 * Header guard...
 */
#ifndef UART_H_
#define UART_H_

/*
 * This will initialize the UART module to asynchronous mode at 9600baud.
 * INPUT: none
 * OUTPUT: none
 */
void initUart(void);

/*
 * Reads a byte from the UART module.
 * INPUT: none
 * OUTPUT: none
 */
char readByteUART(void);
//void sendByteUART(char data);

#endif	/* UART_H_ */

