/*
 * NAME: DataScreen.c
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file defines all functionality for a data screen. A data
 * screen in this project is the screen that displays all raw data output and
 * the processed results from the GPS and Altimeter sensors. This screen is
 * mostly for debugging and as such it will only refresh when a touch has been
 * detected anywhere outside of the "home" button. This file has a lot of
 * "magic" numbers; these numbers have been ascertained experimentally and, due
 * to their arbitrary nature, they have not been defined.
 */

#include "HomeScreen.h" // initHomeScreen
#include "LCDDriver.h" // low level gfx control
#include "CapTouchDriver.h" // TOUCH_EVENT
#include "MPLDriver.h" // MPL_ALT_RAW and drivers
#include "EEPROM.h" // EEPROM_read/write
#include "Gfx.h" // COORDINATE
#include "RiffRaff.h" // *_UNITS, BITMASK_*, Colors[*], SCREEN
#include <stdio.h> // sprintf
#include <stdbool.h>
#include <xc.h> // delay
#include "GPSDriver.h" // all gps related stuff

/*
 * Version number of the software. This number should match the git tags.
 */
#define SOFTWARE_REVISION 1.8

/*
 * From Google: number of feet in a meter
 */
#define FT_PER_METER 3.28084

/*
 * This isn't actually required for this screen to operate but is required to
 * pass as input to the other screens init functions AND setting this struct's
 * data update func to NULL will prevent the timer from updating this.
 */
static SCREEN *screen;

/*
 * Update all of the data on this screen. This is a long run-on function but is
 * that way to avoid costly function calls. Since stuff up to the end of the
 * function depends on data from previous things that have happened, recreating
 * this functionality in a new function would probably be costly in memory and
 * speed. TODO: confirm that; this is long and ugly
 * INPUT: none
 * OUTPUT: none
 */
static void refreshData() {
    // begin with a bulk wipe of the screen - everything below the home button
    // goes
    floodLCD(Colors[WHITE],
            0,
            WIDTH_PIXELS,
            HOME_BUTTON_Y + HOME_BUTTON_HEIGHT,
            HEIGHT_PIXELS);

    // initialize the text stuff in gfx.h
    tCoordinate.x = 0;
    tCoordinate.y = HOME_BUTTON_Y + HOME_BUTTON_HEIGHT + 1;
    tSize = 1;

    // print the raw and processed altitude/temperature data
    tColor = Colors[RED];
    printf("  ALTIMETER\n");
    tColor = Colors[GREEN];
    printf("Raw Data\n");

    MPL_ALT_RAW raw_alt;
    readAltitudeRawMPL(&raw_alt); // get bytes from alt registers on MPL

    tColor = Colors[BLACK];
    printf("Altitude: MSB: 0x%X, CSB: 0x%X, LSB: 0x%X\n",
            raw_alt.msb,
            raw_alt.csb,
            raw_alt.lsb);

    MPL_TEMP_RAW raw_temp;
    readTemperatureRawMPL(&raw_temp); // get bytes from temp registers on MPL
    printf("Temperature: MSB: 0x%X, LSB: 0x%X\n", raw_temp.msb, raw_temp.lsb);

    // process the raw alt/temp values and display
    tColor = Colors[GREEN];
    printf("Processed Data\n");
    tColor = Colors[BLACK];
    printf("Altitude: %f%c, Temperature: %f%c%c\n",
            rawAltToFloatMPL(&raw_alt, METRIC_UNITS),
            'm',
            rawTempToFloatMPL(&raw_temp, METRIC_UNITS),
            248,
            'C');
    printf("Altitude: %f%s, Temperature: %f%c%c\n",
            rawAltToFloatMPL(&raw_alt, IMPERIAL_UNITS),
            "ft",
            rawTempToFloatMPL(&raw_temp, IMPERIAL_UNITS),
            248,
            'F');

    // print raw and processed GPS info
    GPS_DATA data;
    char buf[256];
    tCoordinate.x = 4;
    tColor = Colors[RED];
    printf("  GPS\n");
    tColor = Colors[GREEN];
    printf("Raw Data\n");

    // read each of the 3 GPS sentences and process them into a GPS_DATA struct
    // GNGNS
    tColor = Colors[BLACK];
    readSentenceGPS(GNGNS, buf);
    parseGNSData(&data, buf);
    printf("%s", buf);

    // GNGSA
    readSentenceGPS(GNGSA, buf);
    parseGSAData(&data, buf);
    printf("%s", buf);

    // GNRMC
    readSentenceGPS(GNRMC, buf);
    parseRMCData(&data, buf);
    printf("%s", buf);

    tColor = Colors[GREEN];
    printf("Processed Data\n");

    // if the GPS was able to provide a date/time, display it as:
    // hour:minute:second[am:pm] [CST:UTC], day/month/year [CST:UTC]
    if (BITMASK_CHECK(data.fixType, DATE_TIME_FIX)) {
        tColor = Colors[BLACK];
        // print UTC (24hr), CST (24hr), CST (12hr) - easier and shorter to type
        // as a for loop
        for (char i = 0; i < 3; ++i) {
            // print time
            printf("%i:%02i:%02.0f",
                    data.dateTime.hour,
                    data.dateTime.minute,
                    data.dateTime.second);
            // print the time's meta
            if (TYPE_12 == data.dateTime.type)
                printf("%s", (data.dateTime.amPm == AM) ? "am" : "pm");
            if (CST == data.dateTime.locale)
                printf(" CST");
            if (UTC == data.dateTime.locale)
                printf(" UTC");

            // print date
            printf(", %i/%i/%i",
                    data.dateTime.day,
                    data.dateTime.month,
                    data.dateTime.year);
            // date meta
            if (CST == data.dateTime.locale) printf(" CST");
            if (UTC == data.dateTime.locale) printf(" UTC");

            // if this is the first time/date, print a trailing ', ' and convert
            // the date/time to 24hr-CST format
            if (0 == i) {
                printf(", ");
                utcToLocal(&data.dateTime, CST); // modifies dateTime in place
            }
            // if this is the second time/date, print a new line because the
            // screen doesn't have room for 3 and convert to 12hr-CST format
            else if (1 == i) {
                printf("\n");
                convert24To12(&data.dateTime); // modifies dateTime in place
            }
        }
        // finished printing date/time
        printf("\n");
    }
    // no date/time to print, tell the user that
    else printf("Acquiring GPS time fix....\n");

    // now try to print the processed lat/long, altitude and num satellites if
    // available
    if (BITMASK_CHECK(data.fixType, LAT_LONG_FIX)) {
        printf("latitude: %i%c, %fmin %c\n",
                data.latDeg,
                248,
                data.latMin,
                (data.latDir == NORTH) ? 'N' : 'S');
        printf("longitude: %i%c, %fmin %c\n",
                data.lonDeg,
                248,
                data.lonMin,
                (data.lonDir == EAST) ? 'E' : 'W');
        printf("Altitude: %fm, %fft\n",
                data.altitude,
                data.altitude * FT_PER_METER);
        printf("Number of satellites used for data: %i\n", data.satsUsed);
    }
    // if the GPS hasn't yet acquired a lat/long fix, then tell the user that
    else printf("Acquiring GPS data fix....\n");
    // attempt to print the speed, course, and DOP's - if they aren't available
    // do nothing since "Acquiring GPS data fix..." will have already been
    // printed if a fix isn't available
    if (BITMASK_CHECK(data.fixType, SPEED_COURSE_FIX)) {
        printf("Horizontal Speed: %.3fkn, %.3fkm/h, %.3fmi/h\n",
                data.speed,
                data.speed * 1.852,
                data.speed * 1.151);
        printf("Course: %f%c (CW from true north)\n", data.cog, 248);
    }
    if (BITMASK_CHECK(data.fixType, DOP_FIX)) {
        printf("HDOP: %f, VDOP: %f, PDOP: %f", data.HDOP, data.VDOP, data.PDOP);
    }
}

/*
 * Simple: catch a touch on the home button and init the home screen or a touch
 * anywhere else updates the displayed data.
 * INPUT: none
 * OUTPUT: none
 */
void maskTouchDataScreen() {
    // get the coordinates of the touch
    TOUCH_EVENT touch;
    captureTouchEvent(&touch);

    // if the touch was inside the only button, init home screen
    if (HOME_BUTTON_X <= touch.x &&
            HOME_BUTTON_X + HOME_BUTTON_WIDTH >= touch.x &&
            HOME_BUTTON_Y <= touch.y &&
            HOME_BUTTON_Y + HOME_BUTTON_HEIGHT >= touch.y)
        initHomeScreen(screen);
    // otherwise, refresh this screen's data
    else refreshData();
}

/*
 * This will draw the data screen.
 * INPUT: SCREEN *s - used by the data screen only to prevent main() from
 *          calling the updateDataFunc and to direct main() to the appropriate
 *          touch masking function
 * OUTPUT: none
 */
void initDataScreen(SCREEN *s) {
    // start by clearing whatever crap is on the screen - erase it all
    floodLCD(Colors[WHITE], 0, WIDTH_PIXELS, 0, HEIGHT_PIXELS);

    // update the data and mask touch functions
    screen = s;
    screen->maskTouchFunc = maskTouchDataScreen;
    screen->updateDataFunc = NULL;

    // print the only static text in this screen
    tCoordinate.x = HOME_BUTTON_X + HOME_BUTTON_WIDTH + 60;
    tCoordinate.y = 10;
    tSize = 1;
    tColor = Colors[RED];
    printf("Altimeter V%.1f\n\n", SOFTWARE_REVISION);
    tCoordinate.x = HOME_BUTTON_X + HOME_BUTTON_WIDTH + 52;
    printf("Touch to refresh");

    // draw the only button and display our dynamic data
    drawHomeButton();
    refreshData();
}

