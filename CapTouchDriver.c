/*
 * NAME: CapTouchDriver.c
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file defines all of the routines that this project will
 * need to interact with the FT6206 capacitive touch controller. The controller
 * is capable of much more than what it's being used for - future development
 * could make better use of some of these features (touch pressure, gestures,
 * etc...).
 */

#include "CapTouchDriver.h" // TouchEvent
#include "LCDDriver.h" // HEIGHT_PIXELS
#include "I2C.h" // I2C control functions

// addr 38, shifted left by 1 to make room for R/W bit
#define CAP_TOUCH_I2C_ADDR 0x70

// these are the register definitions for the capacitive touch controller - many
// of them are unused but I'll leave here for posterity's sake since I have at
// least one more screen from Adafruit lying around....
#define FT6206_DEV_MODE 0x0
#define FT6206_GEST_ID 0x01
#define FT6206_TD_STATUS 0x02
#define FT6206_P1_XH 0x03
#define FT6206_P1_XL 0x04
#define FT6206_P1_YH 0x05
#define FT6206_P1_YL 0x06
#define FT6206_P1_WEIGHT 0x07
#define FT6206_P1_MISC 0x08
#define FT6206_P2_XH 0x09
#define FT6206_P2_XL 0x0A
#define FT6206_P2_YH 0x0B
#define FT6206_P2_YL 0x0C
#define FT6206_P2_WEIGHT 0x0D
#define FT6206_P2_MISC 0x0E
#define FT6206_TH_GROUP 0x80
#define FT6206_TH_DIFF 0x85
#define FT6206_CTRL 0x86
#define FT6206_TIMEENTERMONITOR 0x87
#define FT6206_PERIODACTIVE 0x88
#define FT6206_PERIODMONITOR 0x89
#define FT6206_RADIAN_VALUE 0x91
#define FT6206_OFFSET_LEFT_RIGHT 0x92
#define FT6206_OFFSET_UP_DOWN 0x93
#define FT6206_DISTANCE_LEFT_RIGHT 0x94
#define FT6206_DISTANCE_UP_DOWN 0x95
#define FT6206_DISTANCE_ZOOM 0x96
#define FT6206_LIB_VER_H 0xA1
#define FT6206_LIB_VER_L 0xA2
#define FT6206_CIPHER 0xA3
#define FT6206_G_MODE 0xA4
#define FT6206_PWR_MODE 0xA5
#define FT6206_FIRMID 0xA6
#define FT6206_FOCALTECH_ID 0xA8
#define FT6206_RELEASE_CODE_ID 0xAF
#define FT6206_STATE 0xBC

/*
 * Reads an 8-bit value from one of the FT6206 registers. The interaction occurs
 * as an I2C read and the transaction is stopped before this returns - this will
 * not support repeated start functionality. Since this program makes minimal
 * use of the FT6206 this start/stop for each bit will suffice.
 * INPUT: const char mem_addr - register location to read from, if the input is
 *          invalid this will return whatever the FT6206 returned
 * OUTPUT: char - a byte of data that represents what is at the input address in
 *          the FT6206
 */
static char readCapTouch(const char mem_addr) {
    unsigned char data;

    // begin I2C transmission with start condition
    i2cStart(CAP_TOUCH_I2C_MODULE);

    // write FT6206 control byte - write
    i2cWrite(CAP_TOUCH_I2C_MODULE, CAP_TOUCH_I2C_ADDR | I2C_WRITE_CMD);

    // write register location to read
    i2cWrite(CAP_TOUCH_I2C_MODULE, mem_addr);

    // send rep-start for data read
    i2cRepStart(CAP_TOUCH_I2C_MODULE);

    // write FT6206 control byte - read
    i2cWrite(CAP_TOUCH_I2C_MODULE, CAP_TOUCH_I2C_ADDR | I2C_READ_CMD);

    // get data
    data = i2cRead(CAP_TOUCH_I2C_MODULE);

    // master send NACK to slave
    i2cMasterAck(CAP_TOUCH_I2C_MODULE, I2C_DATA_NOACK);

    // send stop
    i2cStop(CAP_TOUCH_I2C_MODULE);

    return data;
}

/*
 * This will send a byte to a register in the FT6206. As with readByteCapTouch()
 * this doesn't support repeated starts...
 * INPUT: const char mem_addr - register location to write to
 *        const char data - data to be written to the register
 */
static void writeCapTouch(const char mem_addr, const char data) {
    // Start the I2C Write Transmission
    i2cStart(CAP_TOUCH_I2C_MODULE);
    // Write I2C OP Code
    i2cWrite(CAP_TOUCH_I2C_MODULE, CAP_TOUCH_I2C_ADDR | I2C_WRITE_CMD);
    // Sending the FT6206 8-bit Memory Address Pointer
    i2cWrite(CAP_TOUCH_I2C_MODULE, mem_addr);
    // Write data to FT6206
    i2cWrite(CAP_TOUCH_I2C_MODULE, data);
    // Stop I2C Transmission
    i2cStop(CAP_TOUCH_I2C_MODULE);
}

/*
 * This will initialize the FT6206 from power up. This will set the device into
 * monitor mode immediately, interrupts, and adjust a reasonable threshold for
 * touch detection (determined experimentally).
 * INPUT: none
 * OUTPUT: none
 */
void initCapTouch() {
    // set device to enter monitor mode when no touch detected
    writeCapTouch(FT6206_CTRL, 1);
    // set timeout for entry into monitor mode
    writeCapTouch(FT6206_CTRL, 0);

    writeCapTouch(FT6206_G_MODE, 1); // 1==interrupt mode (vs polling mode...)
    writeCapTouch(FT6206_TH_GROUP, 0x9F); // set touch threshold

    // set report rates
    writeCapTouch(FT6206_PERIODACTIVE, 0xFF);
    writeCapTouch(FT6206_PERIODMONITOR, 0xFF); // default 0x28
}

/*
 * This will get the x and y coordinates of the most recent touch event and
 * place them into the given TOUCH_EVENT. The coordinates will be as old as the
 * most recent touch - it is up to the caller to ensure that this is called ASAP
 * when an interrupt occurs.
 * INPUT: TOUCH_EVENT * const event - pointer to a x and y container - the
 *          pointer will not be modified
 * OUTPUT: none
 */
void captureTouchEvent(TOUCH_EVENT *event) {
    // start with y coordinate

    // contains MSB of y-coord and event flag
    event->y = readCapTouch(FT6206_P1_XH);
    BITMASK_CLEAR(event->y, 0xF0); // get rid of event and null bits
    event->y <<= 8; // place MSB in proper places in short
    event->y |= readCapTouch(FT6206_P1_XL); // get LSB of y touch
    event->y = HEIGHT_PIXELS - event->y;

    // deal with x coordinate

    // contains MSB of x-coord and event flag
    event->x = readCapTouch(FT6206_P1_YH);
    BITMASK_CLEAR(event->x, 0xF0); // get rid of event and null bits
    event->x <<= 8; // place MSB in proper places in short
    event->x |= readCapTouch(FT6206_P1_YL); // get LSB of x touch
}