/*
 * NAME: EEPROM.h
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file defines read and write routines that work with a
 * PIC18LF46K22 (as well as several others: see pg 15 of PIC manual).
 */

/*
 * Header guard...
 */
#ifndef EEPROM_H_
#define	EEPROM_H_

// constants for EEPROM storage locations
#define UNITS_EEPROM 0x0
#define RAW_ALT_MSB_EEPROM 0x1
#define RAW_ALT_CSB_EEPROM 0x2
#define RAW_ALT_LSB_EEPROM 0x3
#define PRECISION_EEPROM 0x4

/*
 * Writes data to EEPROM. This function blocks and will return after the write
 * has completed.
 * INPUT: byte addr - 8-bit address in EEPROM to write the data to
 *        byte data - 8-bits of data to be written
 * OUTPUT: none
 */
void EEPROM_Write(const char addr, const char data);
/*
 * Read a byte from EEPROM.
 * INPUT: char addr - 8-bit address of data EEPROM to read from
 * OUTPUT: char - data contained at input data EEPROM location
 */
char EEPROM_Read(const char addr);

#endif	/* EEPROM_H_ */

