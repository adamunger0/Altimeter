/*
 * NAME: GPSDriver.c
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file will provide the higher level functionality of parsing
 * through the GNGSA, GNGNS, and GNRMC NMEA sentences as well as polling the
 * A5100-A GPS receiver for said sentences. The parsing algorithms are pretty
 * naive in the sense that they comma count from the beginning of each sentence.
 * Without dynamic memory allocation this is really the only option. Since the
 * date and time that are acquired from the GPS satellites are in UTC, this file
 * will also provide the time and date conversion functions.
 */

#include "GPSDriver.h" // all of the structs that are used here
#include <string.h> // strcmp
#include "RiffRaff.h" // HIGH/LOW, BITMASK_*
#include <stdbool.h>
#include <stdlib.h> // atoi, atof
#include <xc.h> // port definitions for GPS pins
#include "UART.h" // the RS232 comm. op's

/*
 * The GPS transceivers WAKEUP pin is on RD3 - it's read only and is only used
 * during the initialization of the transceiver on power up.
 */
#define GPS_WAKEUP PORTDbits.RD3

/*
 * This micro is running at 64MHz but the Microchip code doesn't allow such a
 * large constant. Instead, define half that speed and double the required
 * calls.
 */
#define _XTAL_FREQ 32000000

/*
 * GNGNS values we want: lat, long, num sats used, ASL. This will start at the
 * beginning of the sentence and parse through each comma delimited value. As
 * each value is encountered it will place it's numerical equivalent into the
 * input GPS_DATA. Once the status indicator has been discovered parsing will
 * stop if the status isn't valid.
 * INPUT: GPS_DATA *data - the holder to contain the result
 *        char *sentence - the GNGNS sentence to parse through
 * OUTPUT: none
 */
void parseGNSData(GPS_DATA *data, char *sentence) {
    char position = 0;
    for (char i = 0; '\0' != sentence[i] && '\n' != sentence[i]; ++i) {
        if (',' == sentence[i]) {
            ++position;
            // latitude info - this comma is the one in between the latitude num
            // and it's n/s indicator. the degrees portion could be a 1, 2 or 3
            // digit number so we need to start with the minutes
            if (3 == position && ',' != sentence[i + 1]) {
                // save i since we're about to clobber the hell out of it
                char comma = i;
                // start by getting the direction indicator
                data->latDir = ('N' == sentence[i + 1]) ? NORTH : SOUTH;
                while('.' != sentence[i] && ',' != sentence[i - 1]) --i;
                // we are now pointing at either 1) the decimal, or 2) the first
                // digit past the previous comma (error!!)
                // now we're positioned at the first digit of the minutes
                --i; --i;
                sentence[i + 7] = '\0'; // null the 3rd comma for atof
                // convert 6-dig str to float
                data->latMin = atof(&(sentence[i]));
                sentence[i + 7] = ','; // restore the 3rd comma
                // now get the degrees
                // the digit that should be null for this atoi
                char degEnd = i;
                char temp = sentence[i]; // save digit so it can be restored
                sentence[i] = '\0'; // null for atoi
                while(',' != sentence[i - 1]) --i;
                data->latDeg = atoi(&(sentence[i]));
                sentence[degEnd] = temp;
                i = comma;
            }
            // longitude info - this comma is the one in between the longitude
            // num and it's e/w indicator. the degrees portion could be a 1, 2
            // or 3 digit number so we need to start with the minutes
            else if (5 == position && ',' != sentence[i + 1]) {
                // save i since we're about to clobber the hell out of it
                char comma = i;
                // start by getting the direction indicator
                data->lonDir = ('E' == sentence[i + 1]) ? EAST : WEST;
                while('.' != sentence[i] && ',' != sentence[i - 1]) --i;
                // we are now pointing at either 1) the decimal, or 2) the first
                // digit past the previous comma (error!!)
                // now we're positioned at the first digit of the minutes
                --i; --i;
                sentence[i + 7] = '\0'; // null the 3rd comma for atof
                // convert 6-dig str to float
                data->lonMin = atof(&(sentence[i]));
                sentence[i + 7] = ','; // restore the 3rd comma
                // now get the degrees
                char degEnd = i; // the digit that should be null for this atoi
                char temp = sentence[i]; // save digit so it can be restored
                sentence[i] = '\0'; // null for atoi
                while(',' != sentence[i - 1]) --i;
                data->lonDeg = atoi(&(sentence[i]));
                sentence[degEnd] = temp;
                i = comma;
            }
            // check the status of this data - if both are 'N' then invalid
            else if (6 == position) {
                if ('N' == sentence[++i] && 'N' == sentence[++i]) {
                    BITMASK_CLEAR(data->fixType, LAT_LONG_FIX);
                    //printf("%c%c\n", sentence[i - 1], sentence[i]);
                    return; // no point hanging around if the data is invalid
                }
                BITMASK_SET(data->fixType, LAT_LONG_FIX);
                ++i;
                //printf("%c%c\n", sentence[i - 2], sentence[i - 1]);
            }
            // number of satellites used in fix - always 2 digits
            else if (7 == position && ',' != sentence[i + 1]) {
                ++i;
                sentence[i + 2] = '\0';
                data->satsUsed = atoi(&(sentence[i]));
                sentence[i + 2] = ',';
            }
            // MSL altitude - note this altitude isn't very accurate
            // http://www.xcmag.com/2011/07/gps-versus-barometric-altitude-the-
            // definitive-answer/
            else if (9 == position && ',' != sentence[i + 1]) {
                char start = ++i; // save the start of the first digit
                // fast forward to the next comma
                while (',' != sentence[i]) ++i;
                sentence[i] = '\0'; // null byte for atof
                data->altitude = atof(&(sentence[start]));
                sentence[i] = ',';
                return;
            }
        }
    }
}

/*
 * Only GNGSA values we're interested in: PDOP, HDOP, VDOP. This will start at
 * the beginning of the sentence and parse through each comma delimited value.
 * As each value is encountered it will place it's numerical equivalent into the
 * input GPS_DATA. Once the status indicator has been discovered parsing will
 * stop if the status isn't valid.
 * INPUT: GPS_DATA *data - the holder to contain the result
 *        char *sentence - the GNGSA sentence to parse through
 * OUTPUT: none
 */
void parseGSAData(GPS_DATA *data, char *sentence) {
    char position = 0;
    for (char i = 0; '\0' != sentence[i] && '\n' != sentence[i]; ++i) {
        if (',' == sentence[i]) {
            ++position;
            // fix type: 1 = no fix, 2 = 2D (<4SVs used), 3 = 3D (>3SVs used)
            if (2 == position) {
                ++i;
                if ('1' == sentence[i]) {
                    BITMASK_CLEAR(data->fixType, DOP_FIX);
                    return; // no point in hanging around if data is invalid
                }
                BITMASK_SET(data->fixType, DOP_FIX);
            }
        }
        // grab the DOP values - variable length so start with the only known
        // constant - the next comma and let atoi do the heavy lifting
        else if ('*' == sentence[i]) {
            // find beginning of first DOP value - PDOP
            position = 0;
            while(position != 3) {
                --i;
                if (',' == sentence[i]) ++position;
            }

            position = ++i; // pointer to first digit in PDOP
            while (',' != sentence[i]) ++i;
            sentence[i] = '\0'; // end of PDOP
            data->PDOP = atof(&(sentence[position]));
            sentence[i] = ','; // reset end of PDOP

            position = ++i; // pointer to first digit in HDOP
            while (',' != sentence[i]) ++i;
            sentence[i] = '\0'; // end of HDOP
            data->HDOP = atof(&(sentence[position]));
            sentence[i] = ','; // reset end of HDOP

            position = ++i; // pointer to first digit in VDOP
            while ('*' != sentence[i]) ++i;
            sentence[i] = '\0'; // end of VDOP
            data->VDOP = atof(&(sentence[position]));
            sentence[i] = '*'; // reset end of VDOP

            // the DOP's are the only values we want...
            return;
        }
    }
}

/*
 * For GNRMC sentences we are only interested in: time, speed over ground,
 * course over ground, and date. This will start at the beginning of the
 * sentence and parse through each comma delimited value. As each value is
 * encountered it will place it's numerical equivalent into the input GPS_DATA.
 * This sentence is used for both the date/time and the horizontal speed and
 * bearing which means that if a time is present in the sentence it is deemed
 * to be valid since the transceiver will maintain this data even without a
 * fix. The speed and bearing information validity is dependant upon the status
 * indicator value.
 * INPUT: GPS_DATA *data - the holder to contain the result
 *        char *sentence - the GNRMC sentence to parse through
 * OUTPUT: none
 */
void parseRMCData(GPS_DATA *data, char *sentence) {
    char position = 0;
    BITMASK_CLEAR(data->fixType, DATE_TIME_FIX);
    BITMASK_CLEAR(data->fixType, SPEED_COURSE_FIX);
    for (char i = 0; '\0' != sentence[i] && '\n' != sentence[i]; ++i) {
        if (',' == sentence[i]) {
            ++position;
            // utc time info
            if (1 == position) {
                if (',' != sentence[i + 1]) {
                    BITMASK_SET(data->fixType, DATE_TIME_FIX);
                    data->dateTime.locale = UTC;
                    data->dateTime.type = TYPE_24;
                    ++i;
                    char temp = sentence[i + 2];
                    sentence[i + 2] = '\0'; // add null for atoi
                    // convert the 2-dig str to an int
                    data->dateTime.hour = atoi(&(sentence[i]));
                    sentence[i + 2] = temp; // remove the null
                    i += 2; // set pointer to beginning of minutes
                    temp = sentence[i + 2];
                    sentence[i + 2] = '\0';
                    // convert the 2-dig str to an int
                    data->dateTime.minute = atoi(&(sentence[i]));
                    sentence[i + 2] = temp;
                    i += 2; // set pointer to beginning of seconds
                    sentence[i + 6] = '\0';
                    // convert 6-dig str to float
                    data->dateTime.second = atof(&(sentence[i]));
                    sentence[i + 6] = ',';
                }
            }
            // check the status of this data - 'V' = invalid data (unless we've
            // also found a time...), 'A' = valid data
            else if (2 == position) {
                ++i;
                if ('A' == sentence[i])
                    BITMASK_SET(data->fixType, SPEED_COURSE_FIX);
            }
            // speed over ground - knots - float
            // since the bearing data immediately follows speed data we might as
            // well grab it right now too
            else if (7 == position &&
                    BITMASK_CHECK(data->fixType, SPEED_COURSE_FIX) &&
                    ',' != sentence[i + 1]) {
                char start = ++i; // start digit of float
                while (',' != sentence[i]) ++i; // find end of float
                sentence[i] = '\0'; // null byte for atof
                // convert float str into float
                data->speed = atof(&(sentence[start]));
                sentence[i] = ',';
                // now get the bearing, or course, data which is after comma 8
                start = ++i; // pointer to first digit of course
                while (',' != sentence[i]) ++i; // find end of float
                sentence[i] = '\0'; // null byte for atof
                // convert float str into float
                data->cog = atof(&(sentence[start]));
                sentence[i] = ',';
                --i;
                ++position;
            }
            // utc date - if we've got a time we'll also have a date, even if
            // the rest of this sentence is garbage
            else if (9 == position && ',' != sentence[i + 1]) {
                // start by grabbing the day
                ++i;
                char tmp = sentence[i + 2];
                sentence[i + 2] = '\0';
                data->dateTime.day = atoi(&(sentence[i]));
                sentence[i + 2] = tmp;
                i += 2;
                // now grab the month
                tmp = sentence[i + 2];
                sentence[i + 2] = '\0';
                data->dateTime.month = atoi(&(sentence[i]));
                sentence[i + 2] = tmp;
                i += 2;
                // now get the year
                tmp = sentence[i + 2];
                sentence[i + 2] = '\0';
                data->dateTime.year = atoi(&(sentence[i]));
                sentence[i + 2] = tmp;
                ++i;
            }
        }
    }
}

/*
 * This function will read sentences as received from the GPS and look for the
 * type as input by type. This sentence is recorded into the input buffer whose
 * length is determined by the caller according to whichever type of sentence is
 * expected. The recorded sentence is appended with a '\0' so that it is usable
 * with standard functions that work with strings(i.e. strlen or printf...).
 * NOTE: due to the highly variable and unpredictable nature of the NMEA
 * sentences, it would be a good idea to pass a 256-byte array or to count the
 * maximum possible number of digits and size accordingly - this function does
 * NO error checking with regard to sizing.
 * INPUT: NMEA_OP_MESSAGE type - type of sentence to look for (see GPSDriver.h)
 *        char *buf - a byte array to contain the requested sentence
 * OUTPUT: none
 */
void readSentenceGPS(NMEA_OP_MESSAGE type, char *buf) {
    // enter an infinite loop and look for the five talker letters that
    // characterize the desired sentence immediately following a '$' sign
    while (true) {
        // hold until the beginning of a sentence is found
        while (readByteUART() != '$');
        // the '$' has been discarded so place it manually
        buf[0] = '$';
        // now grab the rest of the sentence - the end game is '\r\n'
        // default to max 255 characters - smarter implementation would use
        // smaller buffers....
        for (char i = 1; i < 255; ++i) {
            buf[i] = readByteUART(); // get single character
            // if the new character is a '\n', then this is the last char so
            // cap the sentence with a string friendly '\0' and return, NOTE:
            // every sentence starts with 5-digit talker id, if a '\n' is found,
            // this sentence has already been vetted against below...
            if ('\n' == buf[i] || 254 == i) {
                buf[i - 1] = '\n';
                buf[i] = '\0'; // good for printing/parsing
                return;
            }
            // if we've got the first five char's of the sentence then let's
            // check it's talker id against what the caller wants - we only
            // support 3 different sentences here
            if (5 == i) {
                bool match = false;
                // necessary for strcmp below, will be overwritten either way
                buf[6] = '\0';
                //printf("%s\n", buf);
                switch (type) {
                    case GNGNS:
                        if (!strcmp(buf, "$GNGNS")) match = true;
                        break;
                    case GNGSA:
                        if (!strcmp(buf, "$GNGSA")) match = true;
                        break;
                    case GNRMC:
                        if (!strcmp(buf, "$GNRMC")) match = true;
                        break;
                }
                // break out of this data gathering for loop and wait until the
                // next sentence if this sentence doesn't match what the caller
                // wants
                if(!match) break;
            }
        }
    }
}

/*
 * The data sheet for the A5100-A says that the ON_OFF pin should be toggled for
 * at least 200ms to make the device active. If this is after a fresh power-up,
 * this shouldn't happen until AFTER the WAKEUP pin has gone high. Additionally,
 * the data sheet states that this could take up to 1 second to take effect.
 * INPUT: none
 * OUTPUT: none
 */
void toggleOnOffGPS() {
    GPS_ON_OFF = LOW;
        // 210ms delay; datasheet recommends 200ms
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        GPS_ON_OFF = HIGH;

        // finally, a 1 second delay to ensure the wakeup or hibernate command
        // has taken effect
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
        __delay_ms(20);
}

/*
 * Initialization will simply assume this is a power up and wait until the
 * WAKEUP pin goes high before turning the device to full power mode. Future
 * versions should also limit the output of the transceiver to only what is
 * required and would even limit the output to polling rather than broadcasting.
 * INPUT: none
 * OUTPUT: none
 */
void initGPS() {
    if (!GPS_WAKEUP) {
        while (!GPS_WAKEUP);
        toggleOnOffGPS();
    }
}

/*
 * This will return the number of days in any given month, accounting for leap-
 * years. The leap year algorithm can be verified at: https://en.wikipedia.org/
 * wiki/Leap_year#Algorithm.
 * INPUT: char month - 1 to 12 corresponding to Jan to Dec
 *        char year - year for leap year calc
 * OUTPUT: char - 28, 29, 30, or 31; days in the given month
 */
static char numDaysMonth(char month, char year) {
    // these months always have 30 days, regardless of leap year status
    if (month == 4 || month == 6 || month == 9 || month == 11) return 30;

    // if this is a leap year then february has 29 days, otherwise 28
    else if (month == 2) {
        // determine if this is a leap year - formula can be verified online
        bool isLeapYear = (year % 4 == 0 && year % 100 != 0) ||
                (year % 400 == 0);
        if (isLeapYear) return 29;
        else return 28;
    }

    // if the previous checks failed then the only other option is 31 days
    else return 31;
}

/*
 * This converts a DATE_TIME struct from 24-hr to 12-hr. This function will
 * overwrite the contents of the given DATE_TIME struct with the new values, if
 * the old ones are desired they need to be backed up before this is called.
 * This assumes that the input is already in 24-hr mode.
 * INPUT: DATE_TIME *dt - a pointer to a date time struct with it's date/time in
 *          24-hr mode.
 * OUTPUT: none
 */
void convert24To12(DATE_TIME *dt) {
    // determine am/pm
    if (11 <= dt->hour) dt->amPm = PM;
    else dt->amPm = AM;

    // adjust the hour value to 1-12 with 0 being 12am
    if (12 < dt->hour) dt->hour -= 12;
    if (0 == dt->hour) dt->hour = 12; // midnight

    dt->type = TYPE_12;
}

/*
 * Will convert a UTC time into a local time. This works even if the input
 * DATE_TIME contains only valid hour, minute and second info. The old info is
 * clobbered and this assumes that the input is in UTC. Also, the locale arg.
 * could be any integer value, even if it isn't defined in LOCALE. NOTE: this
 * doesn't support non-integer offsets.
 * INPUT: DATE_TIME *dt - pointer to struct whose values UTC->locale
 *        LOCALE locale - any integer offset from UTC
 * OUTPUT: none
 */
void utcToLocal(DATE_TIME *dt, LOCALE locale) {
    dt->hour += locale; // account for offset
    // deal with scenario where the offset caused the time to "wrap-around" to a
    // different day....
    if (dt->hour > 23) { // 0 <= h <= 23
        dt->hour = 23 - (0xFF - dt->hour); // wraparound
        --dt->day; // offset for the hour that wrapped into yesterday
        if (0 == dt->day) {
            // now that day has rolled over we need to check which month we're
            // currently in so that we can determine how many days the new month
            // should have. Start by decrementing the month, if the month is 0,
            // then it needs to be reset to 12 and the year decremented;
            // thankfully, the year doesn't logically need to be checked for
            // rollover....
            --dt->month;
            if (0 == dt->month) { // 1 <= m <= 12
                --dt->year;
                dt->month = 12;
            }
            dt->day = numDaysMonth(dt->month, dt->year);
        }
    }

    // update the values in the date time struct to reflect what it contains
    dt->type = TYPE_24;
    dt->locale = locale;
}