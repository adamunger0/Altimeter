/*
 * NAME: Gfx.c
 * AUTHOR: Adam Unger
 * DESCRIPTION: This defines a universal home button creation function, putch
 * (for printf(...)) and a de-pixelating routine for text. To reduce function
 * argument count, the parameters have been defined globally in Gfx.h. This is
 * a tradeoff since it would be easy to forget to set it and have strange things
 * happen but is worth the reduced finger strain....
 */

#include "Gfx.h"
#include <string.h> // strlen
#include <stdio.h> // sprintf
#include <stdlib.h> //atoi
#include <stdbool.h>
#include "glcdfont.h"
#include "LCDDriver.h" // primitive drawing functions for particular screen


/*
 * Draws a standard home button to the screen at the location defined in Gfx.h.
 * INPUT: none
 * OUTPUT: none
 */
void drawHomeButton() {
    // top
    floodLCD(Colors[DARK_BLUE],
            HOME_BUTTON_X + BUTTON_OUTLINE_WIDTH,
            HOME_BUTTON_X + HOME_BUTTON_WIDTH - BUTTON_OUTLINE_WIDTH,
            HOME_BUTTON_Y,
            HOME_BUTTON_Y + BUTTON_OUTLINE_WIDTH);
    // bottom
    floodLCD(Colors[DARK_BLUE],
            HOME_BUTTON_X + BUTTON_OUTLINE_WIDTH,
            HOME_BUTTON_X + HOME_BUTTON_WIDTH - BUTTON_OUTLINE_WIDTH,
            HOME_BUTTON_Y + HOME_BUTTON_HEIGHT - BUTTON_OUTLINE_WIDTH,
            HOME_BUTTON_Y + HOME_BUTTON_HEIGHT);

    // right
    floodLCD(Colors[DARK_BLUE],
            HOME_BUTTON_X + HOME_BUTTON_WIDTH - BUTTON_OUTLINE_WIDTH,
            HOME_BUTTON_X + HOME_BUTTON_WIDTH,
            HOME_BUTTON_Y,
            HOME_BUTTON_Y + HOME_BUTTON_HEIGHT);
    // left
    floodLCD(Colors[DARK_BLUE],
            HOME_BUTTON_X,
            HOME_BUTTON_X + BUTTON_OUTLINE_WIDTH,
            HOME_BUTTON_Y,
            HOME_BUTTON_Y + HOME_BUTTON_HEIGHT);

    // now flood the buttons background with a lighter shade of blue
    floodLCD(Colors[BLUE],
            HOME_BUTTON_X + BUTTON_OUTLINE_WIDTH,
            HOME_BUTTON_X + HOME_BUTTON_WIDTH - BUTTON_OUTLINE_WIDTH,
            HOME_BUTTON_Y + BUTTON_OUTLINE_WIDTH,
            HOME_BUTTON_Y + HOME_BUTTON_HEIGHT - BUTTON_OUTLINE_WIDTH);

    // write the static text for this button
    tCoordinate.x = 19;
    tCoordinate.y = 10;
    tSize = 1;
    tColor = Colors[DARK_BLUE];
    printf("Home");
}

/*
 * This will fill in the corners of a character as represented by the input bool
 * buffer where a true in a "pixel location" (actually an index into this 2D
 * array) indicates the pixel is lit up. It will fill the corners by adding a
 * square to corners that is 1/4 size of textsize*textsize.
 *
 * e.g. size 8 font:
 *          00000000
 *          00000000
 *          00000000
 *          00000000
 *          00000000****
 *          00000000****     *the de-pixelated stuff fills in the crevice
 *          00000000****
 *          00000000****
 *          0000000000000000
 *          0000000000000000
 *          0000000000000000
 *          0000000000000000
 *          0000000000000000
 *          0000000000000000
 *          0000000000000000
 *          0000000000000000
 *
 * INPUT: bool buf[CHAR_WIDTH][CHAR_HEIGHT] - a boolean array representing each
 *  non-scaled pixel of the character
 * OUTPUT: none
 */
static void dePixelate(bool buf[CHAR_WIDTH][CHAR_HEIGHT],
                       short x,
                       short y,
                       char size,
                       Color c) {
    // To fill in the corners we will fill with blocks that are half as big as
    // the blocks that the current text size would create. This will be an
    // awkward calculation if the current text size isn't an even number, so for
    // odd number text sizes we round up since during testing this size looked
    // better than rounding down
    char nTextSize = (size % 2) ? ((char) size / 2) + 1 : size / 2;

    // intermediate x and y coordinates during calculations in for loops below
    short xc, yc;

    // each pixel will check for shading on the left therefore we can start with
    // the second column
    for (char i = 1; i < CHAR_WIDTH; ++i) {
        for (char j = 0; j < CHAR_HEIGHT; ++j) {
            // if j==0, can only check for left bottom shading and bottom left
            // shading
            // else if j==CHAR_HEIGHT, can only check top left and left top
            // shading
            // else check for top left, left top, left bottom and bottom left
            // shading
            if (buf[i][j]) {
                // deal with the top left and left top situations
                if (j && buf[i - 1][j - 1]) {
                    // shade left top if necessary
                    if (!buf[i - 1][j]) {
                        xc = x + (size * i) - nTextSize;
                        yc = y + (size * j);
                        floodLCD(c, xc, xc + nTextSize, yc, yc + nTextSize);
                    }
                    // shade top left if necessary
                    if (!buf[i][j - 1]) {
                        xc = x + (size * i);
                        yc = y + (size * j) - nTextSize;
                        floodLCD(c, xc, xc + nTextSize, yc, yc + nTextSize);
                    }
                }
                // deal with bottom left and left bottom situations
                if (j < CHAR_HEIGHT - 1 && buf[i - 1][j + 1]) {
                    // shade left bottom if necessary
                    if (!buf[i - 1][j]) {
                        xc = x + (size * i) - nTextSize;
                        yc = y + (size * (j + 1)) - nTextSize;
                        floodLCD(c, xc, xc + nTextSize, yc, yc + nTextSize);
                    }
                    // shade bottom left if necessary
                    if (!buf[i][j + 1]) {
                        xc = x + (size * i);
                        yc = y + (size * (j + 1));
                        floodLCD(c, xc, xc + nTextSize, yc, yc + nTextSize);
                    }
                }
            }
        }
    }
}

/*
 * This is called behind the scenes by printf(...). printf(...) will parse the
 * input string and it's arguments into ASCII char's and call this with each
 * char. The input char will be printed according to the param's as defined
 * above. It is up to the caller to set the above location, size, and color to
 * expected values BEFORE printf(...) is called. A newline will increment the
 * coordinate to: y = (textsize * CHAR_HEIGHT) + 1, and x = 0. No other escape
 * char's are recognized and may result in *odd* output. See glcdfont.h for the
 * byte array for the ascii and extended ascii char set. The algorithm used has
 * been adapted from Google search's and the Adafruit arduino code. I'm
 * convinced that there isn't a better algorithm when working with a small
 * micro and limited memory - the next best option is font files and (possibly)
 * vector graphics.
 * INPUT: char ch - char to be printed
 * OUTPUT: none
 */
void putch(char ch) {
    // if newline just update coordinates and return
    if ('\n' == ch) {
        tCoordinate.y += tSize * CHAR_HEIGHT + CHAR_SPACING;
        tCoordinate.x = 0;
        return;
    }
    // insert newline if string is running off of edge of screen
    if (tCoordinate.x > WIDTH_PIXELS - tSize * CHAR_WIDTH) {
        tCoordinate.y += tSize * CHAR_HEIGHT + CHAR_SPACING;
        tCoordinate.x = 0;
    }
    // buffer to contain true in positions where the pixel is lit as a result of
    // being part of the character and false otherwise - used in dePixelate()
    bool buf[CHAR_WIDTH][CHAR_HEIGHT];

    // container for each column of the char
    char charColumn;
    // this outer loop will execute CHAR_WIDTH times since each byte of the
    // character array represents a column of the character
    for (char i = 0; i < CHAR_WIDTH; ++i) { // i == which column of the char
        charColumn = CHAR_SET[i + (CHAR_WIDTH * ch)];
        // iterate through each bit in the charColumn - each bit represents
        // pixel on or off: 1==on, 0==off
        for (char j = 0; j < CHAR_HEIGHT; ++j) { // j == which row of the char
            if (charColumn & 0x1) {
                buf[i][j] = true;
                short xc = tCoordinate.x + (i * tSize);
                short yc = tCoordinate.y + (j * tSize);
                floodLCD(tColor, xc, xc + tSize, yc, yc + tSize);
            }
            else buf[i][j] = false;
            charColumn >>= 1;
        }
    }

    // make the text less blocky looking to smooth it out
    if (tSize > 1) dePixelate(buf, tCoordinate.x, tCoordinate.y, tSize, tColor);
    // increment the coordinates since this coord is currently occupied by the
    // char that was just drawn
    tCoordinate.x += tSize * CHAR_WIDTH + CHAR_SPACING;
}
