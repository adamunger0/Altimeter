/*
 * NAME: PinMagic.c
 * AUTHOR: Adam Unger
 * DESCRIPTION: Initialize all of the ports on the PIC18LF46K22.
 *  PINS: RA0: D0 LCD digital output low
 *		    1: D1 LCD digital output low
 *		    2: D2 LCD digital output low
 *		    3: D3 LCD digital output low
 *		    4: D4 LCD digital output low
 *		    5: D5 LCD digital output low
 *		    6: D6 LCD digital output low
 *		    7: D7 LCD digital output low
 *		  RB0: INT0 - Cap Touch IRQ digital input high
 *		    1:
 *		    2:
 *		    3:
 *		    4:
 *		    5:
 *		    6: PGC:
 *		    7: PGD:
 *		  RC0: RD LCD digital output low
 *		    1:
 *		    2: LITE LCD digital output high (PWM) (pin low = lite on)
 *		    3: SCL1 Cap Touch digital input low; MSSP1 pg 242
 *		    4: SDA1 Cap Touch digital input low; MSSP1 pg 242
 *		    5: WR LCD digital output low
 *		    6: D/C LCD digital output low
 *		    7:
 *		  RD0: SCL2 MPL3115A2 I2C digital input low; MSSP2 pg 242
 *		    1: SDA2 MPL3115A2 I2C digital input low; MSSP2 pg 242
 *		    2: GPS ON/OFF digital output low
 *		    3: GPS WAKEUP digital input low
 *		    4:
 *		    5:
 *		    6: TX Maestro digital output low; EUSART2
 *		    7: RX Maestro digital input low; EUSART2
 *		  RE0:
 *		    1:
 *		    2:
 *		    3: MCLR:
 *		    4: N/A
 *		    5: N/A
 *		    6: N/A
 *		    7: N/A
 */

#include "PinMagic.h" // REFRESH_CONSTANT
#include "RiffRaff.h" // HIGH, LOW
#include <xc.h> // register definitions

/*
 * Initialize the interrupts for high interrupt on cap. touch and low interrupt
 * on timer0 (for data gathering reminders...).
 * INPUT: none
 * OUTPUT: none
 */
static void setupInterrupts() {
    // set global interrupt options
    INTCONbits.PEIE = HIGH; // enable peripheral interrupts - timers, ADC, etc
    RCONbits.IPEN = HIGH; // enable priority interrupts

    // interrupt INT0 for cap touch - defaults to high priority
    INTCON2bits.INTEDG0 = LOW; // cause interrupt at falling edge
    INTCONbits.INT0IE = HIGH; // enable int 0 (RB0)
    INTCONbits.INT0IF = LOW; // reset interrupt flag

    // interrupt for timer0
    TMR0 = REFRESH_CONSTANT_SLOW_L;
    TMR0L = REFRESH_CONSTANT_SLOW_H;
    T0CON = 0x87;
    INTCON2bits.TMR0IP = LOW;
    INTCONbits.TMR0IE = HIGH;
    INTCONbits.TMR0IF = LOW;
}

/*
 * Initialize the ports on the PIC micro. See description of this file for
 * explanations.
 * INPUT: none
 * OUTPUT: none
 */
static void initPorts() {
    // set the ports as analog or digital
    ANSELA = 0;
    ANSELB = 0;
    ANSELC = 0;
    ANSELD = 0;
    ANSELE = 0;

    // set the initial output value to be written to the ports when they're TRIS'd
    PORTA = 0;
    PORTB = 0b00000010;
    PORTC = 0b00000000;
    PORTD = 0;
    PORTE = 0;

    // TRIS the ports as input or output
    TRISA = 0;
    TRISB = 0b00000001;
    TRISC = 0b00011000;
    TRISD = 0b11001011;
    TRISE = 0;
}

/*
 * This will setup the micro to function as expected for the altimeter project.
 * This includes a 64MHz clock speed, enter sleep mode on SLEEP(), init ports,
 * and init timers.
 */
void setupMicro() {
    initPorts();

    // setup oscillator for 64MHz operation
    OSCCONbits.IDLEN = LOW; // enter sleep mode on sleep instruction
    OSCCONbits.HFIOFS = HIGH; // HFINTOSC is stable
    OSCCONbits.IRCF = 7; // HF internal clock - 16MHz * 4 (PLL) = 64MHz
    OSCTUNEbits.PLLEN = HIGH; // 4x multiplier enabled

    setupInterrupts();
}
