/*
 * NAME: DataScreen.h
 * AUTHOR: Adam Unger
 * DESCRIPTION: The interface for all functionality related to drawing a data
 * screen. See DataScreen.c for a more complete description of the data screen.
 * The only function that needs to be called for this screen to properly
 * function is the initialization screen. Everything else is handled
 * autonomously.
 */

/*
 * Header guard....
 */
#ifndef DATASCREEN_H_
#define	DATASCREEN_H_

#include "RiffRaff.h" // SCREEN

void initDataScreen(SCREEN *screen);

#endif	/* DATASCREEN_H_ */

