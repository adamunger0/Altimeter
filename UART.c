/*
 * NAME: UART.c
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file defines the interaction with the UART module.
 */

#include <xc.h> // register and pin definitions
#include "RiffRaff.h" // HIGH/LOW


/*
 * This will initialize the UART module to asynchronous mode at 9600baud.
 * INPUT: none
 * OUTPUT: none
 */
void initUart() {
    // enable the receiver and transmitter
    TXSTA2bits.SYNC = LOW; // asynchronous mode
    TXSTA2bits.BRGH = HIGH; // high speed

    // set up the baud rate - BR = Fosc / (4 * ((SPBRGH2:SPBRG2) + 1))
    BAUDCON2bits.BRG16 = HIGH; // 16-bit baud rate timer
    TXSTA2bits.BRGH = HIGH; // high baud rate

    SPBRGH2 = 0x6;
    SPBRG2 = 0x81;
}

/*
 * Reads a byte from the UART module.
 * INPUT: none
 * OUTPUT: none
 */
char readByteUART() {
    RCSTA2bits.SPEN = HIGH; // serial port enabled
    RCSTA2bits.CREN = HIGH; // receiver enabled

    // wait until the interrupt flag indicates that a new byte is available
    while (HIGH != PIR3bits.RC2IF) {
        // read the RCSTA2 reg. to clear any framing errors
        char dummy = RCSTA2;
        // clear overflow errors while waiting to complete the bit transfer
        // it would be strange to get an overflow error here since the u is
        // running at 64MHz and the baud rate is 9.6kHz.....
        if (dummy & 0x2) {
            RCSTA2bits.CREN = LOW;
            RCSTA2bits.CREN = HIGH;
        }
    }

    RCSTA2bits.CREN = LOW; // disable receiver to avoid overflow errors
    RCSTA2bits.SPEN = LOW; // serial port disabled

    return RCREG2;
}

//void sendByteUART(char data) {
//    RCSTA2bits.SPEN = HIGH; // serial port enabled
//    TXSTA2bits.TXEN = HIGH; // transmitter enabled
//
//    TXREG2 = data;
//
//    // wait until the register is clear before disabling the serial port
//    while (HIGH != PIR3bits.TX2IF);
//
//    TXSTA2bits.TXEN = LOW; // disable transmitter
//    RCSTA2bits.SPEN = LOW; // serial port disabled
//}
