/*
 * NAME: SportScreen.c
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file defines the sport screen of the altimeter. This screen
 * is supposed to display the altitude, speeds, and coordinates in a simple and
 * high contrast manner. Note that there are a lot of magic numbers scattered
 * haphazardly throughout this file - they are experimentally derived and due to
 * there arbitrary nature they have not been defined.
 */

#include "HomeScreen.h" // initHomeScreen
#include "LCDDriver.h" // low level gfx control
#include "CapTouchDriver.h" // TOUCH_EVENT
#include "MPLDriver.h" // MPL_ALT_RAW and drivers
#include "EEPROM.h" // read/write
#include "Gfx.h" // high level gfx stuff
#include <stdio.h> // printf
#include <stdbool.h>
#include "PinMagic.h" // REFRESH_CONSTANT_500MS
#include "GPSDriver.h" // read the GPS driver

/*
 * Define the coordinates of the two buttons on this screen
 */
#define PRECISION_BUTTON_X 60
#define PRECISION_BUTTON_Y 0
#define PRECISION_BUTTON_WIDTH 80
#define PRECISION_BUTTON_HEIGHT 40

/*
 * Used to toggle the red/green 4x4 square in the upper right corner which is
 * mostly just used for debugging purposes.
 */
bool test;

/*
 * Meta-data for the function of this screen
 */
static SCREEN *screen;
/*
 * One or two digits of precision
 */
static char precision;
/*
 * Hang on to the previous altitude so that we can prevent redrawing digits that
 * are already on the screen.
 */
static char prevNum[8];
/*
 * Hang on to the previous units so that we can prevent redrawing the units.
 */
static char prevUnits;

/*
 * Draw the precision button. This simply toggles between zero or one digit of
 * precision on the displayed altitude.
 * INPUT: none
 * OUTPUT: none
 */
static void drawPrecisionButton() {
    // top
    floodLCD(Colors[DARK_GREEN],
            PRECISION_BUTTON_X + BUTTON_OUTLINE_WIDTH,
            PRECISION_BUTTON_X + PRECISION_BUTTON_WIDTH - BUTTON_OUTLINE_WIDTH,
            PRECISION_BUTTON_Y,
            PRECISION_BUTTON_Y + BUTTON_OUTLINE_WIDTH);
    // bottom
    floodLCD(Colors[DARK_GREEN],
            PRECISION_BUTTON_X + BUTTON_OUTLINE_WIDTH,
            PRECISION_BUTTON_X + PRECISION_BUTTON_WIDTH - BUTTON_OUTLINE_WIDTH,
            PRECISION_BUTTON_Y + PRECISION_BUTTON_HEIGHT - BUTTON_OUTLINE_WIDTH,
            PRECISION_BUTTON_Y + PRECISION_BUTTON_HEIGHT);

    // right
    floodLCD(Colors[DARK_GREEN],
            PRECISION_BUTTON_X + PRECISION_BUTTON_WIDTH - BUTTON_OUTLINE_WIDTH,
            PRECISION_BUTTON_X + PRECISION_BUTTON_WIDTH,
            PRECISION_BUTTON_Y,
            PRECISION_BUTTON_Y + PRECISION_BUTTON_HEIGHT);

    // left
    floodLCD(Colors[DARK_GREEN],
            PRECISION_BUTTON_X,
            PRECISION_BUTTON_X + BUTTON_OUTLINE_WIDTH,
            PRECISION_BUTTON_Y,
            PRECISION_BUTTON_Y + PRECISION_BUTTON_HEIGHT);

    // now flood the buttons background with a lighter shade of blue
    floodLCD(Colors[GREEN],
            PRECISION_BUTTON_X + BUTTON_OUTLINE_WIDTH,
            PRECISION_BUTTON_X + PRECISION_BUTTON_WIDTH - BUTTON_OUTLINE_WIDTH,
            PRECISION_BUTTON_Y + BUTTON_OUTLINE_WIDTH,
            PRECISION_BUTTON_Y + PRECISION_BUTTON_HEIGHT - BUTTON_OUTLINE_WIDTH);

    // write the static text for this button: title and sub-heading
    tCoordinate.x = 74;
    tCoordinate.y = 10;
    tSize = 1;
    tColor = Colors[DARK_GREEN];
    printf("Precision");

    // the precision as its stored in the EEPROM matches what the user will see
    tCoordinate.x = 68;
    tCoordinate.y = 22;
    printf("%i dec. pts.", precision);
}

/*
 * TODO: this will be tricky since during previous testing it was seen that due
 * to the small intervals of time between samples and the (relatively) high
 * error between altitude samples.
 */
static void printVerticalVelocity() {

}

/*
 * This will print the altitude to the screen. It works by converting the
 * altitude to an 8-digit string. The string is printed to the screen one digit
 * at a time starting with the right-most digit. If a digit is already on the
 * screen (as indicated by the prevNum variable at the top of this file) this
 * will skip printing to save time.
 * NOTE: trying to split this into different functions was attempted but the
 * program size and data usage jumped dramatically - this is better for speed...
 * INPUT: float altitude - the current altitude to be displayed
 * OUTPUT: none
 */
static void printAlt(float altitude) {
    // the altitude *could* be up to 8 digits long, min 3 (with decimal)
    char buf[9]; // digits + null
    sprintf(buf, "%08.1f", altitude);

    // start by printing the units to the screen if they aren't already there
    if (prevUnits != screen->units) {
        tCoordinate.x = 270;
        tCoordinate.y = 135;
        tSize = 3;
        tColor = Colors[WHITE];
        floodLCD(Colors[BLACK],
                tCoordinate.x,
                tCoordinate.x + 31,
                tCoordinate.y,
                tCoordinate.y + 21);
        if (METRIC_UNITS == screen->units) printf("m");
        else if (IMPERIAL_UNITS == screen->units) printf("ft");
        prevUnits = screen->units;
    }

    // doing this will prevent this digit from printing next time...
    if (PRECISION_0 == precision) buf[7] = 0;

    // tenths - will always be present and will always print if not a duplicate
    if (buf[7] != prevNum[7]) {
        prevNum[7] = buf[7];
        tCoordinate.x = 244;
        tCoordinate.y = 142;
        tSize = 2;
        tColor = Colors[GRAY];
        floodLCD(Colors[BLACK],
                tCoordinate.x,
                tCoordinate.x + 21,
                tCoordinate.y,
                tCoordinate.y + 14);
        if (PRECISION_1 == precision) printf(".%c", buf[7]);
    }
    // ones will always be present and will always print if not a duplicate
    if (buf[5] != prevNum[5]) {
        prevNum[5] = buf[5];
        tCoordinate.x = 213;
        tCoordinate.y = 114;
        tSize = 6;
        tColor = Colors[RED];
        floodLCD(Colors[BLACK],
                tCoordinate.x,
                tCoordinate.x + 30,
                tCoordinate.y,
                tCoordinate.y + 42);
        putch(buf[5]);
    }
    // tens - skip if same as prev
    if (buf[4] != prevNum[4] ||
            buf[3] != prevNum[3] ||
            buf[2] != prevNum[2] ||
            buf[1] != prevNum[1]) {
        prevNum[4] = buf[4];
        tCoordinate.x = 182;
        tCoordinate.y = 114;
        tSize = 6;
        tColor = Colors[RED];
        floodLCD(Colors[BLACK],
                tCoordinate.x,
                tCoordinate.x + 30,
                tCoordinate.y,
                tCoordinate.y + 42);
        // if digit is number then definitely print it or if it is 0 then print
        // 0 if there are more digits
        if ('0' != buf[4] || '0' != buf[3] || '0' != buf[2] || '0' != buf[1])
            putch(buf[4]);
    }
    // hundreds - skip if same as prev
    if (buf[3] != prevNum[3] || buf[2] != prevNum[2] || buf[1] != prevNum[1]) {
        prevNum[3] = buf[3];
        tCoordinate.x = 151;
        tCoordinate.y = 114;
        tSize = 6;
        tColor = Colors[RED];
        floodLCD(Colors[BLACK],
                tCoordinate.x,
                tCoordinate.x + 30,
                tCoordinate.y,
                tCoordinate.y + 42);
        // if digit is number then definitely print it or if it is 0 then print
        // 0 if there are more digits
        if ('0' != buf[3] || '0' != buf[2] || '0' != buf[1]) putch(buf[3]);
    }
    // thousands - skip if same as prev
    if (buf[2] != prevNum[2] || buf[1] != prevNum[1]) {
        prevNum[2] = buf[2];
        tCoordinate.x = 110;
        tCoordinate.y = 100;
        tSize = 8;
        tColor = Colors[WHITE];
        floodLCD(Colors[BLACK],
                tCoordinate.x,
                tCoordinate.x + 40,
                tCoordinate.y,
                tCoordinate.y + 56);
        // if digit is number then definitely print it or if it is 0 then print
        // 0 if there are more digits
        if ('0' != buf[2] || '0' != buf[1]) putch(buf[2]);
    }
    // ten thousands - skip if same as prev
    if (buf[1] != prevNum[1]) {
        prevNum[1] = buf[1];
        tCoordinate.x = 69;
        tCoordinate.y = 100;
        tSize = 8;
        tColor = Colors[WHITE];
        floodLCD(Colors[BLACK],
                tCoordinate.x,
                tCoordinate.x + 40,
                tCoordinate.y,
                tCoordinate.y + 56);
        // if digit is number then definitely print it or if it is 0 then print
        // 0 if there are more digits
        if ('0' != buf[1]) putch(buf[1]);
    }
    // clear the negative sign position
    prevNum[0] = buf[0];
    tCoordinate.x = 28;
    tCoordinate.y = 100;
    floodLCD(Colors[BLACK], tCoordinate.x,
            tCoordinate.x + 40,
            tCoordinate.y,
            tCoordinate.y + 56);

    // print negative sign if it exists and only if the number isn't zero - i.e.
    // if the number is <1 but decimals aren't being shown, then don't
    // print the negative sign
    if ('0' != buf[0]) {
        tColor = Colors[RED];
        // print it in the 10's spot
        if ('0' == buf[4] && '0' == buf[3] && '0' == buf[2] && '0' == buf[1]) {
            tCoordinate.x = 182;
            tCoordinate.y = 114;
            tSize = 6;
            floodLCD(Colors[BLACK],
                    tCoordinate.x,
                    tCoordinate.x + 30,
                    tCoordinate.y,
                    tCoordinate.y + 42);
        }
        // print it in the 100's spot
        else if ('0' == buf[3] && '0' == buf[2] && '0' == buf[1]) {
            tCoordinate.x = 151;
            tCoordinate.y = 114;
            tSize = 6;
            floodLCD(Colors[BLACK],
                    tCoordinate.x,
                    tCoordinate.x + 30,
                    tCoordinate.y,
                    tCoordinate.y + 42);
        }
        // print it in the 1000's spot
        else if ('0' == buf[2] && '0' == buf[1]) {
            tCoordinate.x = 110;
            tCoordinate.y = 100;
            tSize = 8;
            tColor = Colors[WHITE];
            floodLCD(Colors[BLACK],
                    tCoordinate.x,
                    tCoordinate.x + 40,
                    tCoordinate.y,
                    tCoordinate.y + 56);
        }
        // print it in the 10000's spot
        else if ('0' == buf[1]) {
            tCoordinate.x = 69;
            tCoordinate.y = 100;
            tSize = 8;
            tColor = Colors[WHITE];
            floodLCD(Colors[BLACK],
                    tCoordinate.x,
                    tCoordinate.x + 40,
                    tCoordinate.y,
                    tCoordinate.y + 56);
        }
        // print it in its special spot - already been cleared
        else {
            tColor = Colors[WHITE];
            tCoordinate.x = 28;
            tCoordinate.y = 100;
            tSize = 8;
        }
        // only print if there is a non-zero digit to go with it
        if ((altitude >= 1 || PRECISION_1 == precision)) printf("-");
    }
}

/*
 * This will update the altitude, speeds, and location data on this screen.
 * INPUT: none
 * OUTPUT: none
 */
void updateDataSportScreen() {
    tCoordinate.x = 316;
    tCoordinate.y = 0;
    if (test) tColor = Colors[GREEN];
    else tColor = Colors[RED];
    test = !test;
    floodLCD(tColor,
            tCoordinate.x,
            tCoordinate.x + 4,
            tCoordinate.y,
            tCoordinate.y + 4);

    // get the altitude
    float altitude = readAltitudeMPL(screen->units);
    float os = rawAltToFloatMPL(&screen->altOffset, screen->units);
    altitude = altitude - os;

    // get the gps data
    GPS_DATA gpsData;
    char buf[256];
    readSentenceGPS(GNGNS, buf);
    parseGNSData(&gpsData, buf);

//    readSentenceGPS(GNGSA, buf);
//    parseGSAData(&gpsData, buf);

    readSentenceGPS(GNRMC, buf);
    parseRMCData(&gpsData, buf);

    // if the gps is providing reliable altitude data then use it
//    if (BITMASK_CHECK(gpsData.fixType, LAT_LONG_FIX) &&
//            2 > gpsData.HDOP &&
//            2 > gpsData.VDOP &&
//            2 > gpsData.PDOP) {
//        // convert to proper units
//        if (IMPERIAL_UNITS == screen->units) gpsData.altitude *= 3.28084;
//        altitude += gpsData.altitude - os;
//        altitude /= 2;
//    }

    printAlt(altitude);
    printVerticalVelocity();

    tSize = 1;
    tColor = Colors[WHITE];
    tCoordinate.x = 40;
    tCoordinate.y = 200;
    floodLCD(Colors[BLACK],
            tCoordinate.x,
            tCoordinate.x + 240,
            tCoordinate.y,
            tCoordinate.y + 7);
    if (BITMASK_CHECK(gpsData.fixType, SPEED_COURSE_FIX)) {
        if (METRIC_UNITS == screen->units)
            printf("Horizontal: %.1fkm/hr", gpsData.speed * 1.852);
        if (IMPERIAL_UNITS == screen->units)
            printf("Horizontal: %.1fmph", gpsData.speed * 1.151);
        printf(", %f%c (CW)\n", gpsData.cog, 248);
    }
    tCoordinate.x = 40;
    tCoordinate.y = 208;
    floodLCD(Colors[BLACK],
            tCoordinate.x,
            tCoordinate.x + 240,
            tCoordinate.y,
            tCoordinate.y + 7);
    if (BITMASK_CHECK(gpsData.fixType, LAT_LONG_FIX)) {
        printf("%i%c %fmin %c, ",
                gpsData.latDeg, 248,
                gpsData.latMin,
                (gpsData.latDir == NORTH) ? 'N' : 'S');
        printf("%i%c %fmin %c\n",
                gpsData.lonDeg, 248,
                gpsData.lonMin,
                (gpsData.lonDir == EAST) ? 'E' : 'W');
    }
}

/*
 * This will check if a touch event has occurred within the bounds of either the
 * home button or the precision button and act accordingly. It not in those
 * bounds, the touch is ignored.
 * INPUT: none
 * OUTPUT: none
 */
void maskTouchSportScreen() {
    TOUCH_EVENT touch;
    captureTouchEvent(&touch);

    // check if the touch occurred inside the home button, if so, change screens
    if (HOME_BUTTON_X <= touch.x &&
            HOME_BUTTON_X + HOME_BUTTON_WIDTH >= touch.x &&
            HOME_BUTTON_Y <= touch.y &&
            HOME_BUTTON_Y + HOME_BUTTON_HEIGHT >= touch.y)
        initHomeScreen(screen);

    // if the touch happened inside the precision button then toggle the level
    // of precision between 0 or 1 and update the displayed data
    else if (PRECISION_BUTTON_X <= touch.x &&
            PRECISION_BUTTON_X + PRECISION_BUTTON_WIDTH >= touch.x &&
            PRECISION_BUTTON_Y <= touch.y &&
            PRECISION_BUTTON_Y + PRECISION_BUTTON_HEIGHT >= touch.y) {
        if (PRECISION_0 == precision) {
            EEPROM_Write(PRECISION_EEPROM, PRECISION_1);
            precision = PRECISION_1;
        }
        else if (PRECISION_1 == precision) {
            EEPROM_Write(PRECISION_EEPROM, PRECISION_0);
            precision = PRECISION_0;
        }
        drawPrecisionButton();
        updateDataSportScreen();
    }
}

/*
 * This will draw the sport screen. The purpose of this screen is to display
 * only the most pertinent info to the user and keeping it up to date with a
 * fast refresh rate.
 * INPUT: SCREEN *s - meta data for this screen
 * OUTPUT: none
 */
void initSportScreen(SCREEN *s) {
    floodLCD(Colors[BLACK], 0, WIDTH_PIXELS, 0, HEIGHT_PIXELS);

    screen = s;
    screen->maskTouchFunc = maskTouchSportScreen;
    screen->updateDataFunc = updateDataSportScreen;

    precision = EEPROM_Read(PRECISION_EEPROM);

    for (char i = 0; i < 8; ++i) prevNum[i] = 0;
    prevUnits = -1;

    screen->tmrL = 0;
    screen->tmrH = 0;

    test = false;

    drawHomeButton();
    drawPrecisionButton();

    updateDataSportScreen();
}
