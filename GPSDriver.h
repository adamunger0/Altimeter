/*
 * NAME: GPSDriver.h
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file is the driver for a Maestro A5100-A GPS transceiver.
 */

/*
 * Header guard...
 */
#ifndef GPSDRIVER_H_
#define	GPSDRIVER_H_

/*
 * The GPS on/off pin is connected to RD2 on the micro. This pin toggles the
 * receiver between hibernate mode and full-power mode.
 *     hibernate: 60uA
 *     full power: 34mA (average), 40mA (peak)
 */
#define GPS_ON_OFF PORTDbits.RD2

/*
 * Provides a convenient mechanism for time keeping in 12-hr mode.
 */
typedef enum {
    AM,
    PM
} AM_PM;

/*
 * Convenient mechanism for determining if the time/date values are in 12 or
 * 24 hr mode.
 */
typedef enum {
    TYPE_24,
    TYPE_12
} DATE_TIME_TYPE;

/*
 * Bit masks for the 4 different types of fixes that this program is interested
 * in acquiring. These four fixes will give all of the information that this
 * program is interested in. See NMEA_OP_MESSAGE below...
 */
typedef enum {
    DATE_TIME_FIX = 0x1,
    LAT_LONG_FIX = 0x2,
    DOP_FIX = 0x4,
    SPEED_COURSE_FIX = 0x8
} FIX_TYPE;

// Offsets are local time relative to UTC time - NOTE: there ARE tz's that
// aren't integer offsets from GMT so a better version of this would probably
// use a LUT...
typedef enum {
    CST = -6,
    UTC = 0
} LOCALE;

/*
 * A struct for keeping track of the date and time. It has provisions for 12 and
 * 24 hour and timezones.
 */
typedef struct {
    unsigned short year;
    char month; // char's default to unsigned, all others - signed
    char day;
    char hour;
    char minute;
    float second;
    AM_PM amPm;
    LOCALE locale;
    DATE_TIME_TYPE type;
} DATE_TIME;

// Talker ID's:
//  GP***: data is from GPS only
//  GL***: data is from GLONASS only
//  GB***: data is from BDS only
//  GN***: data is from multiple constellations
typedef enum {
    // lat, long, num sats used, ASL, geoid separation
    GNGNS,
    // PDOP, HDOP, VDOP
    GNGSA,
    // time, speed over ground, course over ground, and date
    GNRMC,
} NMEA_OP_MESSAGE;


/*
 * Convenience to store directional information.
 */
typedef enum {
    NORTH,
    SOUTH,
    EAST,
    WEST
} DIRECTION;

/*
 * This structure encapsulates all of the pertinent GPS info that this program
 * may have an interest in seeing. It can be used as a container for all of that
 * info or portions of. The fixType variable can be masked to determine what, if
 * any information is accurate.
 */
typedef struct {
    DATE_TIME dateTime;
    char latDeg; // degrees
    float latMin; // minutes
    DIRECTION latDir; // N or S
    char lonDeg; // degrees
    float lonMin; // minutes
    DIRECTION lonDir; // E or W
    char satsUsed; // number of satellites used for this data
    char fixType; // type of position fix - indicates if fix was valid
    float speed; // knots, ground speed
    float cog; // degrees clockwise from geographic north - Course Over Ground
    float altitude; // meters - MSL
    // for these *DOP's see: https://en.wikipedia.org/wiki/Dilution_of_precision
    // _(GPS)
    // <1: Ideal: Highest possible confidence level to be used for
    // applications demanding the highest possible precision at all times.
    // 1-2: Excellent: At this confidence level, positional measurements are
    // considered accurate enough to meet all but the most sensitive
    // applications.
    // 2-5: Good: Represents a level that marks the minimum appropriate for
    // making business decisions. Positional measurements could be used to make
    // reliable in-route navigation suggestions to the user.
    // 5-10: Moderate: Positional measurements could be used for calculations,
    // but the fix quality could still be improved. A more open view of the sky
    // is recommended.
    // 10-20: Fair: Represents a low confidence level. Positional measurements
    // should be discarded or used only to indicate a very rough estimate of the
    // current location.
    // >20: Poor: At this level, measurements are inaccurate by as much as 300
    // meters with a 6-meter accurate device (50 DOP � 6 meters) and should be
    // discarded.
    float HDOP; // horizontal dilution of precision
    float VDOP; // vertical dilution of precision
    float PDOP; // position dilution of precision
} GPS_DATA;

/*
 * The data sheet for the A5100-A says that the ON_OFF pin should be toggled for
 * at least 200ms to make the device active. If this is after a fresh power-up,
 * this shouldn't happen until AFTER the WAKEUP pin has gone high.
 * INPUT: none
 * OUTPUT: none
 */
void toggleOnOffGPS(void);

/*
 * Initialization will simply assume this is a power up and wait until the
 * WAKEUP pin goes high before turning the device to full power mode. Future
 * versions should also limit the output of the transceiver to only what is
 * required and would even limit the output to polling rather than broadcasting.
 * INPUT: none
 * OUTPUT: none
 */
void initGPS(void);

/*
 * Only GNGSA values we're interested in: PDOP, HDOP, VDOP. This will start at
 * the beginning of the sentence and parse through each comma delimited value.
 * As each value is encountered it will place it's numerical equivalent into the
 * input GPS_DATA. Once the status indicator has been discovered parsing will
 * stop if the status isn't valid.
 * INPUT: GPS_DATA *data - the holder to contain the result
 *        char *sentence - the GNGSA sentence to parse through
 * OUTPUT: none
 */
void parseGSAData(GPS_DATA *data, char *sentence);

/*
 * GNGNS values we want: lat, long, num sats used, ASL. This will start at the
 * beginning of the sentence and parse through each comma delimited value. As
 * each value is encountered it will place it's numerical equivalent into the
 * input GPS_DATA. Once the status indicator has been discovered parsing will
 * stop if the status isn't valid.
 * INPUT: GPS_DATA *data - the holder to contain the result
 *        char *sentence - the GNGNS sentence to parse through
 * OUTPUT: none
 */
void parseGNSData(GPS_DATA *data, char *sentence);

/*
 * For GNRMC sentences we are only interested in: time, speed over ground,
 * course over ground, and date. This will start at the beginning of the
 * sentence and parse through each comma delimited value. As each value is
 * encountered it will place it's numerical equivalent into the input GPS_DATA.
 * This sentence is used for both the date/time and the horizontal speed and
 * bearing which means that if a time is present in the sentence it is deemed
 * to be valid since the transceiver will maintain this data even without a
 * fix. The speed and bearing information validity is dependant upon the status
 * indicator value.
 * INPUT: GPS_DATA *data - the holder to contain the result
 *        char *sentence - the GNRMC sentence to parse through
 * OUTPUT: none
 */
void parseRMCData(GPS_DATA *data, char *sentence);

/*
 * This function will read sentences as received from the GPS and look for the
 * type as input by type. This sentence is recorded into the input buffer whose
 * length is determined by the caller according to whichever type of sentence is
 * expected. The recorded sentence is appended with a '\0' so that it is usable
 * with standard functions that work with strings(i.e. strlen or printf...).
 * NOTE: due to the highly variable and unpredictable nature of the NMEA
 * sentences, it would be a good idea to pass a 256-byte array or to count the
 * maximum possible number of digits and size accordingly - this function does
 * NO error checking with regard to sizing.
 * INPUT: NMEA_OP_MESSAGE type - type of sentence to look for (see GPSDriver.h)
 *        char *buf - a byte array to contain the requested sentence
 * OUTPUT: none
 */
void readSentenceGPS(NMEA_OP_MESSAGE type, char *buf);

/*
 * This converts a DATE_TIME struct from 24-hr to 12-hr. This function will
 * overwrite the contents of the given DATE_TIME struct with the new values, if
 * the old ones are desired they need to be backed up before this is called.
 * This assumes that the input is already in 24-hr mode.
 * INPUT: DATE_TIME *dt - a pointer to a date time struct with it's date/time in
 *          24-hr mode.
 * OUTPUT: none
 */
void convert24To12(DATE_TIME *dt);

/*
 * Will convert a UTC time into a local time. This works even if the input
 * DATE_TIME contains only valid hour, minute and second info. The old info is
 * clobbered and this assumes that the input is in UTC. Also, the locale arg.
 * could be any integer value, even if it isn't defined in LOCALE. NOTE: this
 * doesn't support non-integer offsets.
 * INPUT: DATE_TIME *dt - pointer to struct whose values UTC->locale
 *        LOCALE locale - any integer offset from UTC
 * OUTPUT: none
 */
void utcToLocal(DATE_TIME *dt, LOCALE locale);

#endif	/* GPSDRIVER_H_ */