/*
 * NAME: RiffRaff.h
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file defines random stuff that doesn't really belong in
 * other files.
 */

/*
 * Header guard...
 */
#ifndef RIFFRAFF_H_
#define	RIFFRAFF_H_

#include "MPLDriver.h" // MPL_ALT_RAW

#define HIGH 1
#define LOW 0

// conveniences
#define METRIC_UNITS 1
#define IMPERIAL_UNITS 2
#define PRECISION_0 0
#define PRECISION_1 1

// x = target variable
#define BITMASK_SET(x, mask) ((x) |= (mask))
#define BITMASK_CLEAR(x, mask) ((x) &= (~(mask)))
#define BITMASK_FLIP(x, mask) ((x) ^= (mask))
#define BITMASK_CHECK(x, mask) (((x) & (mask)) == (mask))
#define BITMASK_MASK(x, mask) ((x) &= (mask))

/*
 * Define a holder for RGB values. The LED has been initialized in 18-bit color
 * mode, i.e. each color is 6-bits.
 */
typedef struct {
    char r : 6;
    char g : 6;
    char b : 6;
} Color;

/*
 * Index's into the colors array. TODO: better would be: #define RED Colors[0]
 */
#define RED 0
#define DARK_RED 1
#define BLUE 2
#define DARK_BLUE 3
#define GREEN 4
#define DARK_GREEN 5
#define YELLOW 6
#define ORANGE 7
#define DARK_ORANGE 8
#define PINK 9
#define DARK_PINK 10
#define PURPLE 11
#define WHITE 12
#define BLACK 13
#define GRAY 14

/*
 * Array of colors. These have been determined from the internet and
 * experimentation.
 */
Color Colors[] = { { 0x3C, 0, 0 },
                   { 0x33, 0, 0 },
                   { 0x8, 0x24, 0x3F },
                   { 0x7, 0x22, 0x3C },
                   { 0x0, 0x3B, 0x0 },
                   { 0x0, 0x33, 0x0 },
                   { 0x3F, 0x3F, 0x0 },
                   { 0x3F, 0x29, 0x0 },
                   { 0x3D, 0x24, 0x0 },
                   { 0x3F, 0x5, 0x25 },
                   { 0x3C, 0x5, 0x22 },
                   { 0x3C, 0x5, 0x22 }, // TODO
                   { 0x3F, 0x3F, 0x3F },
                   { 0, 0, 0 },
                   { 0x3A, 0x3A, 0x3A } };

/*
 * Meta-data container for the screens to pass around to each other to keep
 * values in memory rather than many EEPROM reads. This also holds the call
 * backs for data gathering and touch masking.
 */
typedef struct {
    char units; // METRIC or IMPERIAL
    // altitude offset in raw register values as found on the MPL
    MPL_ALT_RAW altOffset;
    // data gathering timer values
    char tmrL;
    char tmrH;
    // callbacks for touch's and timer timeouts
    void (*updateDataFunc)(void);
    void (*maskTouchFunc)(void);
} SCREEN;

#endif	/* RIFFRAFF_H_ */

