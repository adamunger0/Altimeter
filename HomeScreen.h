/*
 * NAME: HomeScreen.h
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file give the interface for initializing the home screen.
 * The home screen will handle everything else itself internally and no other
 * calls need be made.
 */

/*
 * Header guard...
 */
#ifndef HOMESCREEN_H_
#define	HOMESCREEN_H_

#include "RiffRaff.h" // SCREEN

/*
 * This function will initialize the screen to the home button state. It starts
 * by clearing anything that may currently be on the screen. Then it updates its
 * meta data (units, offsets, etc) and finally updates the values on the screen.
 * INPUT: SCREEN *s - the meta data for this screen
 * OUTPUT: none
 */
void initHomeScreen(SCREEN *s);

#endif	/* HOMESCREEN_H_ */

