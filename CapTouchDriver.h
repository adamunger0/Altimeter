/*
 * FILE: CapTouchDriver.h
 * AUTHOR: Adam Unger
 * DESCRIPTION: This is the interface for the capacitive touch controls. Very
 * simple: just initialize the controller and get a touch point.
 */

/*
 * Header guards....
 */
#ifndef CAPTOUCHDRIVER_H_
#define	CAPTOUCHDRIVER_H_

/*
 * A struct to capture a touch event - same as COORDINATE but the name is more
 * intuitive. The FT6206 is capable of much more than what we're using it for
 * so having this separate will allow future expansion to be simpler. Since a
 * touch coord can't be negative, the var's are unsigned...
 */
typedef struct {
    unsigned short x;
    unsigned short y;
} TOUCH_EVENT;

/*
 * This will initialize the FT6206 from power up. This will set the device into
 * monitor mode immediately, interrupts, and adjust a reasonable threshold for
 * touch detection (determined experimentally).
 * INPUT: none
 * OUTPUT: none
 */
void initCapTouch(void);

/*
 * This will get the x and y coordinates of the most recent touch event and
 * place them into the given TOUCH_EVENT. The coordinates will be as old as the
 * most recent touch - it is up to the caller to ensure that this is called ASAP
 * when an interrupt occurs.
 * INPUT: TOUCH_EVENT *event - pointer to a x and y container - the
 *          pointer will not be modified
 * OUTPUT: none
 */
void captureTouchEvent(TOUCH_EVENT *event);

#endif	/* CAPTOUCHDRIVER_H_ */

