/*
 * NAME: HomeScreen.c
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file defines the functionality related to the home screen.
 * The home screen is the default screen that is displayed on power up and is
 * the "central" screen that the other two screens go to. It provides date and
 * time info and all of the buttons for implementing the expected functionality
 * of the altimeter. Note that there are a lot of magic numbers scattered
 * haphazardly throughout this file - they are experimentally derived and due to
 * there arbitrary nature they have not been defined.
 */

#include <stdbool.h>
#include <stdio.h> // NULL
#include "CapTouchDriver.h" // TOUCH_EVENT
#include "Gfx.h" // high level gfx functions
#include "LCDDriver.h" // floodLCD, BACKLIGHT
#include "EEPROM.h"
#include "MPLDriver.h"
#include "GPSDriver.h" // read/write from/to EEPROM
#include "MPLDriver.h" // off mpl
#include "SportScreen.h" // initSportScreen
#include "DataScreen.h" // initDataScreen
#include <xc.h> // SLEEP, intcon
#include "PinMagic.h" // refresh constant

/*
 * This micro is running at 64MHz but the Microchip code doesn't allow such a
 * large constant. Instead, define half that speed and double the required
 * calls.
 */
#define _XTAL_FREQ 32000000

/*
 * Coordinates of the buttons for the home screen. The x and y are the top left
 * corner of the button and height is down from the top while width is right
 * from the left (duh...).
 */

#define BUTTON_DEPTH 3

#define INFO_BUTTON_X 10 // pixels
#define INFO_BUTTON_Y 10 // pixels
#define INFO_BUTTON_WIDTH 180 // pixels
#define INFO_BUTTON_HEIGHT 76 // pixels

#define UNITS_BUTTON_X 200 // pixels
#define UNITS_BUTTON_Y 10 // pixels
#define UNITS_BUTTON_WIDTH 110 // pixels
#define UNITS_BUTTON_HEIGHT 76 // pixels

#define SPORT_BUTTON_X 10 // pixels
#define SPORT_BUTTON_Y 96 // pixels
#define SPORT_BUTTON_WIDTH 300 // pixels
#define SPORT_BUTTON_HEIGHT 60 // pixels

#define ZERO_BUTTON_X 10 // pixels
#define ZERO_BUTTON_Y 166 // pixels
#define ZERO_BUTTON_WIDTH 180 // pixels
#define ZERO_BUTTON_HEIGHT 64 // pixels

#define POWER_BUTTON_X 200 // pixels
#define POWER_BUTTON_Y 166 // pixels
#define POWER_BUTTON_WIDTH 110 // pixels
#define POWER_BUTTON_HEIGHT 64 // pixels

/*
 * Tells the status of the on or off of the LED screen.
 */
static bool power;

/*
 * Holds screen state info.
 */
static SCREEN *screen;

/*
 * Draw the info button. This button displays the date, time, altitude and
 * the temperature. This data is updated in the updatedata function.
 * INPUT: none
 * OUTPUT: none
 */
static void drawInfo() {
    // start with the bottom and right lines of the button for a shadow effect
    // bottom horizontal line
    floodLCD(Colors[DARK_GREEN],
            INFO_BUTTON_X + BUTTON_DEPTH,
            INFO_BUTTON_X + INFO_BUTTON_WIDTH,
            INFO_BUTTON_Y + INFO_BUTTON_HEIGHT,
            INFO_BUTTON_Y + INFO_BUTTON_HEIGHT + BUTTON_DEPTH);
    // right vertical line
    floodLCD(Colors[DARK_GREEN],
            INFO_BUTTON_X + INFO_BUTTON_WIDTH,
            INFO_BUTTON_X + INFO_BUTTON_WIDTH + BUTTON_DEPTH,
            INFO_BUTTON_Y + BUTTON_DEPTH,
            INFO_BUTTON_Y + BUTTON_DEPTH + INFO_BUTTON_HEIGHT);

    // now flood the buttons background with a lighter shade of blue
    floodLCD(Colors[GREEN],
            INFO_BUTTON_X,
            INFO_BUTTON_X + INFO_BUTTON_WIDTH,
            INFO_BUTTON_Y,
            INFO_BUTTON_Y + INFO_BUTTON_HEIGHT);

    // write the static text for this button: title and sub-heading
    tCoordinate.x = 36;
    tCoordinate.y = 20;
    tSize = 2;
    tColor = Colors[WHITE];
    printf("Current Data");
    tCoordinate.x = 40;
    tCoordinate.y = 35;
    tSize = 1;
    printf("Touch here to see all");
}

/*
 * Draw the button for toggling between metric or imperial units.
 * INPUT: none
 * OUTPUT: none
 */
static void drawUnitsButton() {
    // start with the bottom and right lines of the button for a shadow effect
    // bottom horizontal line
    floodLCD(Colors[DARK_GREEN],
            UNITS_BUTTON_X + BUTTON_DEPTH,
            UNITS_BUTTON_X + UNITS_BUTTON_WIDTH,
            UNITS_BUTTON_Y + UNITS_BUTTON_HEIGHT,
            UNITS_BUTTON_Y + UNITS_BUTTON_HEIGHT + BUTTON_DEPTH);
    // right vertical line
    floodLCD(Colors[DARK_GREEN],
            UNITS_BUTTON_X + UNITS_BUTTON_WIDTH,
            UNITS_BUTTON_X + UNITS_BUTTON_WIDTH + BUTTON_DEPTH,
            UNITS_BUTTON_Y + BUTTON_DEPTH,
            UNITS_BUTTON_Y + BUTTON_DEPTH + UNITS_BUTTON_HEIGHT);

    // now flood the buttons background with a lighter shade of green
    floodLCD(Colors[GREEN],
            UNITS_BUTTON_X,
            UNITS_BUTTON_X + UNITS_BUTTON_WIDTH,
            UNITS_BUTTON_Y,
            UNITS_BUTTON_Y + UNITS_BUTTON_HEIGHT);

    // write the static text for this button: the text changes depending on what
    // units the user has selected - update the screen according to what is
    // written in EEPROM
    tColor = Colors[WHITE];
    // draw the metric units version of this button
    if (METRIC_UNITS == screen->units) {
        tSize = 2;
        tCoordinate.x = 222;
        tCoordinate.y = 32;
        printf("Metric");

        tSize = 1;
        tCoordinate.x = 214;
        tCoordinate.y = 54;
        printf("Touch here for");

        tCoordinate.x = 232;
        tCoordinate.y = 62;
        printf("Imperial");
    }
    // draw the imperial units version of this button
    else if (IMPERIAL_UNITS == screen->units) {
        tSize = 2;
        tCoordinate.x = 212;
        tCoordinate.y = 32;
        printf("Imperial");

        tSize = 1;
        tCoordinate.x = 212;
        tCoordinate.y = 54;
        printf("Touch here for");

        tCoordinate.x = 237;
        tCoordinate.y = 62;
        printf("Metric");
    }
}

/*
 * Draw the button for switching contexts into sport mode.
 * INPUT: none
 * OUTPUT: none
 */
static void drawSportScreenButton() {
    // start with the bottom and right lines of the button for a shadow effect
    // bottom horizontal line
    floodLCD(Colors[DARK_BLUE],
            SPORT_BUTTON_X + BUTTON_DEPTH,
            SPORT_BUTTON_X + SPORT_BUTTON_WIDTH,
            SPORT_BUTTON_Y + SPORT_BUTTON_HEIGHT,
            SPORT_BUTTON_Y + SPORT_BUTTON_HEIGHT + BUTTON_DEPTH);
    // right vertical line
    floodLCD(Colors[DARK_BLUE],
            SPORT_BUTTON_X + SPORT_BUTTON_WIDTH,
            SPORT_BUTTON_X + SPORT_BUTTON_WIDTH + BUTTON_DEPTH,
            SPORT_BUTTON_Y + BUTTON_DEPTH,
            SPORT_BUTTON_Y + BUTTON_DEPTH + SPORT_BUTTON_HEIGHT);

    // now flood the buttons background with a lighter shade of blue
    floodLCD(Colors[BLUE],
            SPORT_BUTTON_X,
            SPORT_BUTTON_X + SPORT_BUTTON_WIDTH,
            SPORT_BUTTON_Y,
            SPORT_BUTTON_Y + SPORT_BUTTON_HEIGHT);

    // write the static text for this button: title and sub-heading
    tColor = Colors[WHITE];
    tCoordinate.x = 108;
    tCoordinate.y = 112;
    tSize = 2;
    printf("Sport Mode");
    tCoordinate.x = 72;
    tCoordinate.y = 136;
    tSize = 1;
    printf("Touch here to enter Sport Mode");
}

/*
 * Draw the zero button which is used to "zero" the alti. Zeroing means to get
 * the offset from MSL to the users current altitude. This offset is then
 * subtracted from any reading taken to give an altitude relative to the offset
 * altitude. Touching this button will toggle between having an offset or an
 * offset of 0.
 * INPUT: none
 * OUTPUT: none
 */
static void drawZeroButton() {
    // start with the bottom and right lines of the button for a shadow effect
    // bottom horizontal line
    floodLCD(Colors[DARK_BLUE],
            ZERO_BUTTON_X + BUTTON_DEPTH,
            ZERO_BUTTON_X + ZERO_BUTTON_WIDTH,
            ZERO_BUTTON_Y + ZERO_BUTTON_HEIGHT,
            ZERO_BUTTON_Y + ZERO_BUTTON_HEIGHT + BUTTON_DEPTH);
    // right vertical line
    floodLCD(Colors[DARK_BLUE],
            ZERO_BUTTON_X + ZERO_BUTTON_WIDTH,
            ZERO_BUTTON_X + ZERO_BUTTON_WIDTH + BUTTON_DEPTH,
            ZERO_BUTTON_Y + BUTTON_DEPTH,
            ZERO_BUTTON_Y + BUTTON_DEPTH + ZERO_BUTTON_HEIGHT);

    // now flood the buttons background with a lighter shade of blue
    floodLCD(Colors[BLUE],
            ZERO_BUTTON_X,
            ZERO_BUTTON_X + ZERO_BUTTON_WIDTH,
            ZERO_BUTTON_Y,
            ZERO_BUTTON_Y + ZERO_BUTTON_HEIGHT);

    // write the static text for this button: title and sub-headings
    // this button will have one of two states depending on if there is an
    // offset present or not
    // no offset
    if (0 == screen->altOffset.msb &&
            0 == screen->altOffset.csb &&
            0 == screen->altOffset.lsb) {
        tColor = Colors[WHITE];
        tCoordinate.x = 56;
        tCoordinate.y = 181;
        tSize = 2;
        printf("Set Zero");
        tCoordinate.x = 38;
        tCoordinate.y = 205;
        tSize = 1;
        printf("Touch here to zero to\n");
        tCoordinate.x = 38;
        printf("your current altitude");
    }
    // has offset - let the user know what it is
    else {
        tCoordinate.x = 46;
        tCoordinate.y = 181;
        tSize = 2;
        tColor = Colors[RED];
        printf("Clear Zero");
        tCoordinate.x = 30;
        tCoordinate.y = 210;
        tSize = 1;
        tColor = Colors[WHITE];
        float os = rawAltToFloatMPL(&screen->altOffset, screen->units);
        printf("Current Offset: %.1f%s",
                os,
                (METRIC_UNITS == screen->units) ? "m" : "ft");
    }
}

/*
 * Draw the power button. This button will toggle between on and off states for
 * power savings when not in use.
 * INPUT: none
 * OUTPUT: none
 */
static void drawPowerButton() {
    // start with the bottom and right lines of the button for a shadow effect
    // bottom horizontal line
    floodLCD(Colors[DARK_RED],
            POWER_BUTTON_X + BUTTON_DEPTH,
            POWER_BUTTON_X + POWER_BUTTON_WIDTH,
            POWER_BUTTON_Y + POWER_BUTTON_HEIGHT,
            POWER_BUTTON_Y + POWER_BUTTON_HEIGHT + BUTTON_DEPTH);
    // right vertical line
    floodLCD(Colors[DARK_RED],
            POWER_BUTTON_X + POWER_BUTTON_WIDTH,
            POWER_BUTTON_X + POWER_BUTTON_WIDTH + BUTTON_DEPTH,
            POWER_BUTTON_Y + BUTTON_DEPTH,
            POWER_BUTTON_Y + BUTTON_DEPTH + POWER_BUTTON_HEIGHT);

    // now flood the buttons background with a lighter shade of red
    floodLCD(Colors[RED],
            POWER_BUTTON_X,
            POWER_BUTTON_X + POWER_BUTTON_WIDTH,
            POWER_BUTTON_Y,
            POWER_BUTTON_Y + POWER_BUTTON_HEIGHT);

    // write the static text for this button: a power symbol and the word power
    tCoordinate.x = 242;
    tCoordinate.y = 170;
    tSize = 6;
    printf("%c", 176);

    tCoordinate.x = 242;
    tCoordinate.y = 220;
    tSize = 1;
    printf("POWER");
}

/*
 * Update the altitude, temperature and date/time on the home screen.
 * INPUT: none
 * OUTPUT: none
 */
void updateDataHomeScreen() {
    // get the mpl data - altitude and temperature
    MPL_DATA mplData;
    readMPL(&mplData, screen->units);
    float os = rawAltToFloatMPL(&screen->altOffset, screen->units);
    mplData.altitude = mplData.altitude - os;

    // get the gps time and date data
    GPS_DATA gpsData;
    char buf[256];

    // will need altitude and DOP data to use the GPS altitude info
//    readSentenceGPS(GNGNS, buf);
//    parseGNSData(&gpsData, buf);
//
//    readSentenceGPS(GNGSA, buf);
//    parseGSAData(&gpsData, buf);

    readSentenceGPS(GNRMC, buf); // date and time
    parseRMCData(&gpsData, buf);

    // if the gps is providing reliable altitude data then use it to average
    // the altitude with the pressure sensor
//    if (BITMASK_CHECK(gpsData.fixType, LAT_LONG_FIX) &&
//            2 > gpsData.HDOP &&
//            2 > gpsData.VDOP &&
//            2 > gpsData.PDOP) {
//        // convert to proper units
//        if (IMPERIAL_UNITS == screen->units) gpsData.altitude *= 3.28084; // ft / m
//        mplData.altitude += gpsData.altitude - os;
//        mplData.altitude /= 2;
//    }

    // clear the space where the altitude, temperature, and lat/long are to go
    tCoordinate.x = 42;
    tCoordinate.y = 52;

    // erase the line where the altitude and temperature sit
    floodLCD(Colors[GREEN],
            tCoordinate.x,
            tCoordinate.x + 125,
            tCoordinate.y,
            tCoordinate.y + 7);

    // refresh the altitude and temperature
    tColor = Colors[WHITE];
    tSize = 1;
    printf("%.1f%s%s, %.1f%c%c\n",
            mplData.altitude,
            (METRIC_UNITS == screen->units) ? "m" : "ft",
            (0 == os) ? " ASL" : "",
            mplData.temp,
            248,
            (METRIC_UNITS == screen->units) ? 'C' : 'F');

    // erase the entire line where the date and time sit
    tCoordinate.x = 22;
    tCoordinate.y += 8;
    floodLCD(Colors[GREEN],
            tCoordinate.x,
            tCoordinate.x + 163,
            tCoordinate.y,
            tCoordinate.y + 7);

    // refresh the time and date if we have a fix
    if (BITMASK_CHECK(gpsData.fixType, DATE_TIME_FIX)) {
        tColor = Colors[WHITE];
        utcToLocal(&gpsData.dateTime, CST);
        convert24To12(&gpsData.dateTime);
        printf("%i:%02i:%02.0f",
                gpsData.dateTime.hour,
                gpsData.dateTime.minute,
                gpsData.dateTime.second);
        if (TYPE_12 == gpsData.dateTime.type)
            printf("%s", (gpsData.dateTime.amPm == AM) ? "am" : "pm");
        printf(" CST");
        printf(", %i/%i/%i",
                gpsData.dateTime.day,
                gpsData.dateTime.month,
                gpsData.dateTime.year);
        if (CST == gpsData.dateTime.locale) printf(" CST");
    }
    // let user know that we don't have a good enough fix yet
    else printf("Acquiring GPS time fix....");
}

/*
 * This powers on the device. A power on is initiated by: putting the GPS
 * transceiver into full power mode, setting the altitude transducer into active
 * mode and turning the LED backlight on. Note that the micro would have been
 * pulled out of SLEEP mode when it received the cap. touch interrupt.
 * INPUT: none
 * OUTPUT: none
 */
static void powerOn() {
    power = true;
    toggleOnOffGPS();
    screen->updateDataFunc = updateDataHomeScreen;
    initMPL();
    sleepOutLCD();
    // re-enable low priority interrupts
    INTCONbits.GIEL = HIGH;
    BACKLIGHT = LOW; // backlight on
}

/*
 * This powers off the device. Power off is characterized by turning the
 * backlight off, putting the GPS into hibernate mode, the MPL into low power
 * mode and putting the micro to SLEEP.
 * INPUT: none
 * OUTPUT: none
 */
static void powerOff() {
    BACKLIGHT = HIGH; // blacklight off
    // disable low priority interrupts
    INTCONbits.GIEL = LOW;
    power = false;
    screen->updateDataFunc = NULL;
    toggleOnOffGPS();
    offMPL();
    sleepLCD();
    SLEEP();
}

/*
 * Mask a touch event. This is called when the main loop has detected a cap.
 * touch interrupt has occurred. This will mask the touch by getting the x and y
 * coordinates from the cap. touch controller and then determine which button
 * (if any) the touch belongs to.
 * INPUT: none
 * OUTPUT: none
 */
void maskTouchHomeScreen() {
    // first, if the device is powered down then ANY touch anywhere on the
    // screen will cause it to power up
    if (!power) {
        powerOn();
        return;
    }

    // get the touch coordinates
    TOUCH_EVENT touch;
    captureTouchEvent(&touch);

    // handle a touch on the screen->units button by changing the screen->units
    // in EEPROM and forcing a redraw of the screen->units button
    if (UNITS_BUTTON_X <= touch.x &&
            UNITS_BUTTON_X + UNITS_BUTTON_WIDTH >= touch.x &&
            UNITS_BUTTON_Y <= touch.y &&
            UNITS_BUTTON_Y + UNITS_BUTTON_HEIGHT >= touch.y) {
        if (METRIC_UNITS == screen->units) {
            EEPROM_Write(UNITS_EEPROM, IMPERIAL_UNITS);
            screen->units = IMPERIAL_UNITS;
        }
        else if (IMPERIAL_UNITS == screen->units) {
            EEPROM_Write(UNITS_EEPROM, METRIC_UNITS);
            screen->units = METRIC_UNITS;
        }
        // redraw the units button to reflect the change
        drawUnitsButton();
        // update the zero button if it currently has a zero
        if (screen->altOffset.msb ||
                screen->altOffset.csb ||
                screen->altOffset.lsb)
            drawZeroButton();
        // update the other data on the screen to reflect the new units
        updateDataHomeScreen();
    }

    // switch to the data screen if the info button has been selected
    else if (INFO_BUTTON_X <= touch.x &&
            INFO_BUTTON_X + INFO_BUTTON_WIDTH >= touch.x &&
            INFO_BUTTON_Y <= touch.y &&
            INFO_BUTTON_Y + INFO_BUTTON_HEIGHT >= touch.y) {
        initDataScreen(screen);
    }

    // handle the set zero button by taking a current altitude reading and
    // writing the raw value to EEPROM and then updating the set zero button OR
    // by setting the offset to 0; this is supposed to toggle between having an
    // offset and not having an offset
    else if (ZERO_BUTTON_X <= touch.x &&
            ZERO_BUTTON_X + ZERO_BUTTON_WIDTH >= touch.x &&
            ZERO_BUTTON_Y <= touch.y &&
            ZERO_BUTTON_Y + ZERO_BUTTON_HEIGHT >= touch.y) {
        // if the value in offset is 0, then get a reading from MPL and set it
        // as the new offset
        if (0 == screen->altOffset.msb &&
                0 == screen->altOffset.csb &&
                0 == screen->altOffset.lsb)
            readAltitudeRawMPL(&screen->altOffset);
        // if the value in offset isn't 0, set the offset to 0
        else {
            screen->altOffset.msb = 0;
            screen->altOffset.csb = 0;
            screen->altOffset.lsb = 0;
        }
        // write the updated offset to long term memory
        EEPROM_Write(RAW_ALT_MSB_EEPROM, screen->altOffset.msb);
        EEPROM_Write(RAW_ALT_CSB_EEPROM, screen->altOffset.csb);
        EEPROM_Write(RAW_ALT_LSB_EEPROM, screen->altOffset.lsb);
        drawZeroButton();
        updateDataHomeScreen();
    }

    // if this function gets to here then power down the device if the power
    // button was selected
    else if (POWER_BUTTON_X <= touch.x &&
            POWER_BUTTON_X + POWER_BUTTON_WIDTH >= touch.x &&
            POWER_BUTTON_Y <= touch.y &&
            POWER_BUTTON_Y + POWER_BUTTON_HEIGHT >= touch.y)
        powerOff();

    // finally, switch to sport mode if that button was selected
    else if (SPORT_BUTTON_X <= touch.x &&
            SPORT_BUTTON_X + SPORT_BUTTON_WIDTH >= touch.x &&
            SPORT_BUTTON_Y <= touch.y &&
            SPORT_BUTTON_Y + SPORT_BUTTON_HEIGHT >= touch.y)
        initSportScreen(screen);
}

/*
 * This function will initialize the screen to the home button state. It starts
 * by clearing anything that may currently be on the screen. Then it updates its
 * meta data (units, offsets, etc) and finally updates the values on the screen.
 * INPUT: SCREEN *s - the meta data for this screen
 * OUTPUT: none
 */
void initHomeScreen(SCREEN *s) {
    // start by clearing old stuff
    floodLCD(Colors[WHITE], 0, WIDTH_PIXELS, 0, HEIGHT_PIXELS);

    // set the interrupt pointers to this screen's functions
    screen = s;
    screen->maskTouchFunc = maskTouchHomeScreen;
    screen->updateDataFunc = updateDataHomeScreen;

    // update the meta data
    screen->units = EEPROM_Read(UNITS_EEPROM);
    screen->altOffset.msb = EEPROM_Read(RAW_ALT_MSB_EEPROM);
    screen->altOffset.csb = EEPROM_Read(RAW_ALT_CSB_EEPROM);
    screen->altOffset.lsb = EEPROM_Read(RAW_ALT_LSB_EEPROM);

    // this screen doesn't require a quick refresh rate
    screen->tmrL = REFRESH_CONSTANT_SLOW_L;
    screen->tmrH = REFRESH_CONSTANT_SLOW_H;

    power = true;

    // draw all of the buttons and update the data displayed in them
    drawInfo();
    drawUnitsButton();
    drawSportScreenButton();
    drawZeroButton();
    drawPowerButton();

    updateDataHomeScreen();
}


