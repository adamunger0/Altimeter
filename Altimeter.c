/*
 * NAME: Altimeter.c
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file contains main() for the altimeter project. The most
 * important thing that it does is initialize the PIC and all of the peripherals
 * that it controls. It doesn't do much else except catch interrupts and react
 * to those interrupts by calling the appropriate handler.
 */

#include "PICSetup.h" // must be included so that the PIC is initialized right
#include "PinMagic.h" // setup micro
#include <xc.h> // register and port definitions
#include <stdbool.h>
#include "I2C.h" // initI2C
#include "LCDDriver.h" // initLCD
#include "MPLDriver.h" // initMPL
#include "CapTouchDriver.h" // initCapTouch
#include "UART.h" // initUART
#include "GPSDriver.h" // initGPS
#include "HomeScreen.h" // initHomeScreen

/*
 * Flags for the two events that will cause interrupts: timer and cap touch.
 * Since they will be modified asynchronously they are declared volatile.
 */
static volatile bit capTouchInterrupt = LOW;
static volatile bit dataGatherInterrupt = LOW;

/*
 * High ISR is used for cap touch.
 */
static void interrupt highIsr(void) {
    // Check for INT0 (cap touch controller) interrupt flag
    if (INTCONbits.INT0IF) {
        INTCONbits.INT0IF = LOW; // reset interrupt flag
        capTouchInterrupt = HIGH;
    }
} // fast retfie

/*
 * Low ISR is used for timer0 interrupts for data gathering/updates.
 */
static void interrupt low_priority lowIsr(void) {
    if (INTCONbits.TMR0IF) {
        INTCONbits.TMR0IF = LOW; // reset the interrupt flag for timer0
        dataGatherInterrupt = HIGH;
    }
} // retfie

/*
 * Main starting point for the whole program. This will start by initializing
 * everything attached to the micro, including the micro itself, then simply
 * enters an infinite loop where the status of the cap touch and data gather
 * flags are continuously polled.
 * INPUT: none
 * OUTPUT: none
 */
void main() {
    // don't allow any interrupts to disrupt the initialization process since
    // that could make strange things happen if the interrupt happens at a bad
    // time
    di();

    // initialize everything
    setupMicro();
    initI2C();
    initUart();
    initLCD();
    initMPL();
    initCapTouch();
    initGPS();

    // flip the master switch to enable interrupts - game time!
    ei();

    // Contains state info for whichever screen is currently being displayed.
    // The only info that is of use here are the touch and data call-backs.
    SCREEN screen;

    // start by drawing the first screen to be displayed to the user
    initHomeScreen(&screen);

    // infinite loop - will loop for duration of power supply to micro
    while(true) {
        // if the capacitive touch has interrupted - deal with it. Note that
        // this might not happen until several ms after the touch has occurred.
        // The interrupt routine will catch it straight-away, but depending on
        // what the micro is currently doing, this might not happen until some
        // time after the touch occurred.
        if (capTouchInterrupt) {
            // since responsiveness to the users touch is the most important,
            // disable interrupts until the touch event has been dealt with
            di();
            screen.maskTouchFunc(); // the screens set this themselves

            // re-init the int source
            INTCONbits.INT0IF = LOW; // reset interrupt flag
            capTouchInterrupt = LOW;

            // re-enable interrupts
            ei();
        }
        // this is a low level priority interrupt - this will only happen after
        // the micro has dealt with any touch events. If this is in the middle
        // of executing, the high level interrupt will occur but this will be
        // allowed to finish execution before it is dealt with so that no
        // modules lock up due to inattentiveness.
        if (dataGatherInterrupt) {
            // if whatever screen is currently being displayed has a data update
            // routine, this won't be null and we'll call it now
            if (screen.updateDataFunc) screen.updateDataFunc();

            // re-init the timer with whatever values the current screen desires
            TMR0 = screen.tmrH;
            TMR0L = screen.tmrL;

            // re-init the timer and finish
            INTCONbits.TMR0IF = LOW; // reset the interrupt flag for timer0
            dataGatherInterrupt = LOW;
        }
    }
}
