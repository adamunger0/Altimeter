# Altimeter by Adam Unger

##   About:
Software for my technical thesis project. The project is a sport altimeter. The idea is to acquire 3D positional information as well as related information (vertical, horizontal speeds, time, date, temperature, etc) in a package that is clean, simple, and durable. For future iterations of this design it would be great to have some form of on-board storage to log the data as it's received in real-time. The data could then be parsed into the proper file format and displayed/overlayed on Google Earth or used as part of a log-book (or both...).

##   Code was written for:
* PIC18LF46K22
* FT6206 capacitive touch controller
* ILI9341 LED display driver
* MPL3115A2 MEMS pressure transducer
* A5100-A GPS transceiver
