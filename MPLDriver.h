/*
 * NAME: MPLDriver.h
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file defines the interface for interactions with the
 * MPL3115A2.
 */

/*
 * Header guard...
 */
#ifndef MPLDRIVER_H_
#define	MPLDRIVER_H_

/*
 * To contain altitude and temperature data.
 */
typedef struct {
    float altitude;
    float temp;
} MPL_DATA;

/*
 * To contain raw register values that correspond to altitude.
 */
typedef struct {
    char msb;
    char csb;
    char lsb;
} MPL_ALT_RAW;

/*
 * Contains raw register values that correspond to temperature.
 */
typedef struct {
    char msb;
    char lsb;
} MPL_TEMP_RAW;

/*
 * Place the MPL into standby mode.
 * INPUT: none
 * OUTPUT: none
 */
void offMPL(void);

/*
 * Set the MPL to a default state. This includes polling mode, active mode, OSR
 * of 128, and in altimeter mode.
 * INPUT: none
 * OUTPUT: none
 */
void initMPL(void);

/*
 * Read the altitude and temperature data from the MPL. The data is written to
 * the input MPL_DATA struct. The data is converted to the input units.
 * INPUT: MPL_DATA *data - pointer to struct to hold the results of the read
 *        char units - METRIC or IMPERIAL
 * OUTPUT: none
 */
void readMPL(MPL_DATA *data, char units);

/*
 * Reads only the altitude from the MPL. The returned value has been converted
 * to the input units.
 * INPUT: char units - METRIC or IMPERIAL
 * OUTPUT: float - the altitude in meters or feet
 */
float readAltitudeMPL(char units);

/*
 * Reads the altitude but doesn't convert to any units. The altitude is stored
 * on the MPL in 3 registers and this will simply update the input MPL_ALT_RAW
 * struct to reflect those values.
 * INPUT: MPL_ALT_RAW *alt - pointer to struct to contain raw register values
 * OUTPUT: none
 */
void readAltitudeRawMPL(MPL_ALT_RAW *alt);

/*
 * Convert the raw altitude values to an altitude in the given units.
 * INPUT: MPL_ALT_RAW *alt - pointer to struct that contains raw altitude data
 *        char units - METRIC or IMPERIAL
 * OUTPUT: float - the altitude in m or ft
 */
float rawAltToFloatMPL(MPL_ALT_RAW *alt, char units);

/*
 * Reads the temperature values in the MPL. These values are not converted to
 * anything.
 * INPUT: MPL_TEMP_RAW *temp - pointer to struct to hold the temp data
 * OUTPUT: none
 */
void readTemperatureRawMPL(MPL_TEMP_RAW *temp);

/*
 * Convert the input temperature data into engineering units.
 * INPUT: MPL_TEMP_RAW *temp - 2 bytes of raw data
 *        char units - METRIC or IMPERIAL
 * OUTPUT: float - the temperature in C or F
 */
float rawTempToFloatMPL(MPL_TEMP_RAW *temp, char units);

#endif	/* MPLDRIVER_H_ */

