/*
 * NAME: EEPROM.c
 * AUTHOR: Adam Unger
 * DESCRIPTION: Despite what the xc8 compiler manual says about the matter,
 * here's what is actually happening: The __EEPROM_DATA macro IS defined and
 * works as expected but it is only for modifying the EEPROM in the hex file -
 * a reset will not update the EEPROM with these default values. To write/read
 * the EEPROM during runtime, the compiler manual says to use EEPROM_READ() or
 * EEPROM_WRITE(), however, if you look at pic18.h, you will see:
 *  #define EEPROM_READ(addr)	0	// Added only for code portability
 *  #define eeprom_read(addr)	0
 *  #define EEPROM_WRITE(addr, value)	// Added only for code portability
 *  #define eeprom_write(addr, value)
 * so....the manual is wrong and all macro invocations and function calls expand
 * to null....thanks a bunch microchip!
 *
 * This file has been specifically written for a 256-byte data EEPROM. This
 * means that these functions will accept only an 8-bit address. See pg. 15 of
 * the PIC manual for a listing of specifications for different devices. These
 * functions could be easily expanded to work with 1024-byte devices.
 *
 * The following algorithms have been directly lifted from pg. 107 of the PIC
 * manual.
 */

#include <xc.h> // register definitions

/*
 * Writes data to EEPROM. This function blocks and will return after the write
 * has completed.
 * INPUT: const char addr - 8-bit address in EEPROM to write the data to
 *        const char data - 8-bits of data to be written
 * OUTPUT: none
 */
void EEPROM_Write(const char addr, const char data) {
    EEADR = addr;
    EEDATA = data;
    EECON1bits.EEPGD = 0;
    EECON1bits.CFGS = 0;
    EECON1bits.WREN = 1;
    //di(); // disable global interrupts
    EECON2 = 0x55;
    EECON2 = 0xAA;
    EECON1bits.WR = 1;
    //ei(); // re-enable global interrupts
    EECON1bits.WREN = 0;

    // block until write cycle is complete
    while (EECON1bits.WR);
}

/*
 * Read a byte from EEPROM.
 * INPUT: const char addr - 8-bit address of data EEPROM to read from
 * OUTPUT: char - data contained at input data EEPROM location
 */
char EEPROM_Read(const char addr) {
    EEADR = addr;
    EECON1bits.EEPGD = 0;
    EECON1bits.CFGS = 0;
    EECON1bits.RD = 1;

    return EEDATA;
}
