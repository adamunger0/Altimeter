/*
 * NAME: SportScreen.h
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file gives the init function for the sport screen.
 */

/*
 * Header guard...
 */
#ifndef SPORTSCREEN_H_
#define	SPORTSCREEN_H_

#include "RiffRaff.h" // SCREEN

/*
 * This will draw the sport screen. The purpose of this screen is to display
 * only the most pertinent info to the user and keeping it up to date with a
 * fast refresh rate.
 * INPUT: SCREEN *s - meta data for this screen
 * OUTPUT: none
 */
void initSportScreen(SCREEN *screen);

#endif	/* SPORTSCREEN_H_ */

