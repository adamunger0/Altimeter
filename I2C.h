/*
 * NAME: I2C.h
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file gives all of the I2C interface functions.
 */

/*
 * Header guard...
 */
#ifndef I2C_H_
#define	I2C_H_

// I2C Bus Control Definition
#define I2C_DATA_ACK 0
#define I2C_DATA_NOACK 1
#define I2C_WRITE_CMD 0
#define I2C_READ_CMD 1

#define I2C_ACK 0
#define I2C_NOACK 1

/*
 * The capacitive touch controller is on MSSP1 and the MPL3115A2 is on MSSP2.
 */
#define CAP_TOUCH_I2C_MODULE 1
#define MPL3115A2_I2C_MODULE 2

/*
 * This will initialize both of the I2C modules on the PIC micro to 400kHz.
 * INPUT: none
 * OUTPUT: none
 */
void initI2C(void);

/*
 * This will block until the given I2C module is idle. See pg. 259, 260, 261,
 * and 262 of the PIC manual for further clarification.
 * INPUT: char module - MSSP1 or MSSP2, see I2C.h for definitions of each
 * OUTPUT: none
 */
void i2cIdle(char module);

/*
 * Send a start condition of the I2C bus.
 * INPUT: char module - MSSP1 or MSSP2, see I2C.h for definitions of each
 * OUTPUT: none
 */
void i2cStart(char module);

/*
 * Send a repeated start condition over the I2C bus.
 * INPUT: char module - MSSP1 or MSSP2, see I2C.h for definitions of each
 * OUTPUT: none
 */
void i2cRepStart(char module);

/*
 * Send a stop condtion over the I2C bus.
 * INPUT: char module - MSSP1 or MSSP2, see I2C.h for definitions of each
 * OUTPUT: none
 */
void i2cStop(char module);

///*
// * Get the ack bit that was set by the slave.
// * INPUT: char module - MSSP1 or MSSP2, see I2C.h for definitions of each
// * OUTPUT: char - status of ack/nack
// */
//char i2cSlaveAck(char module);

/*
 * Write 8-bits to the I2C port. Assumes that i2cStart() has already been called
 * and that a device is waiting for this message. If data is an address, ensure
 * that the R/W bit is added to the byte.
 * INPUT: char data - data to be written to the SDA line
 * OUTPUT: data written - no return
 */
void i2cWrite(char module, char data);

/*
 * Send the ack condition given over the I2C bus. See I2C.h for definitions of
 * the ack/nack and MSSP modules.
 * INPUT: char module - MSSP1 or MSSP2
 *        char ack_type - ack/nack
 * OUTPUT: none
 */
void i2cMasterAck(char module, char ack_type);

/*
 * Read a byte of data from the I2C bus. The caller is responsible for calling
 * i2cStop() when finished.
 * INPUT: char module - MSSP1 or MSSP2, see I2C.h for definitions of each
 * OUTPUT: char - the byte that the slave sent.
 */
char i2cRead(char module);

#endif	/* I2C_H_ */

