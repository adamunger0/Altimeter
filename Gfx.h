/*
 * NAME: Gfx.h
 * AUTHOR: Adam Unger
 * DESCRIPTION: This file defines the public interface for higher level drawing
 * routines. Due to the importance of things being reasonably quick and agile,
 * the entirety of this project has been conceived to use a few graphics
 * primitives as possible (i.e. circles and other shapes,
 * non-horizontal/vertical lines, shading, etc). This file gives the few simple
 * but necessary "graphics"-y stuff.
 */

/*
 * Header guard...
 */
#ifndef GFX_H_
#define	GFX_H_

#include "RiffRaff.h" // Color

/*
 * Standard ASCII 5x7 font - the width and height scale according to the text
 * size, but the spacings are constant
 */
#define CHAR_WIDTH 5
#define CHAR_HEIGHT 7
#define CHAR_SPACING 1
#define LINE_SPACING 1

/*
 * Determined experimentally that an outline width of 3 looked the best...
 */
#define BUTTON_OUTLINE_WIDTH 3

/*
 * Determined experimentally that the home button looks best with these dims...
 */
#define HOME_BUTTON_X 0
#define HOME_BUTTON_Y 0
#define HOME_BUTTON_WIDTH 60
#define HOME_BUTTON_HEIGHT 40

/*
 * Similar to a TOUCH_EVENT. Simply defines a intersection on the screen. Used
 * in this program to store absolute pixel values, but more abstract uses could
 * be realized. Touch coordinates can't be negative so unsigned...
 */
typedef struct {
    unsigned short x;
    unsigned short y;
} COORDINATE;

/*
 * putch() will use these to determine where to place what it draws and what
 * size and color it will be.
 */
COORDINATE tCoordinate;
char tSize;
Color tColor;

/*
 * Draws a standard home button to the screen at the location defined above.
 * INPUT: none
 * OUTPUT: none
 */
void drawHomeButton(void);

#endif	/* GFX_H_ */

